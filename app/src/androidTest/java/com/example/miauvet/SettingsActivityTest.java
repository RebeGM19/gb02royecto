package com.example.miauvet;

import android.view.Gravity;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import com.example.miauvet.ui.activities.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class SettingsActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void accionSettingsActivity() {

        clickOnYourNavigationItem_ShowsYourScreen(0);

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Cambiamos el idioma de español a ingles
        onView(withText(R.string.preferences_languages_content_title)).perform(click());
        onView(withText(R.string.preferences_languages_english_title)).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        pressBack();

        //Scroll hacia la ultima mascota
        RecyclerView recyclerViewMascota = activityRule.getActivity().findViewById(R.id.list);
        int itemCountMascota = recyclerViewMascota.getAdapter().getItemCount();
        onView(withId(R.id.list)).perform(RecyclerViewActions.scrollToPosition(itemCountMascota - 1));

        //Accedemos a la mascota
        onView(withId(R.id.list)).perform(RecyclerViewActions.actionOnItemAtPosition(itemCountMascota - 1, click()));
        try {
            sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Comprobamos si los textos han cambiado de idioma a ingles
        onView(withId(R.id.textView2)).check(matches(withText("-Name:")));
        onView(withId(R.id.textView4)).check(matches(withText("Pet")));

        //Volvemos a ajustes a traves del navigation drawer
        clickOnYourNavigationItem_ShowsYourScreen(1);

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Cambiamos el idioma de ingles a español
        onView(withText(R.string.preferences_languages_content_title)).perform(click());
        onView(withText(R.string.preferences_languages_spanish_title)).perform(click());
        onView(withContentDescription(R.string.nav_app_bar_navigate_up_description)).perform(click());

        //Scroll hasta la ultima mascota
        onView(withId(R.id.list)).perform(RecyclerViewActions.scrollToPosition(itemCountMascota - 1));

        //Accedemos a la mascota
        onView(withId(R.id.list)).perform(RecyclerViewActions.actionOnItemAtPosition(itemCountMascota - 1, click()));

        try {
            sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Comprobamos si los textos han cambiado de idioma a español
        onView(withId(R.id.textView2)).check(matches(withText("-Nombre:")));
        onView(withId(R.id.textView4)).check(matches(withText("Mascota")));

        pressBack();
    }

    /*
     * Para abrir el navigation drawer, dependiendo del contexto donde te encuentres
     */
    public void clickOnYourNavigationItem_ShowsYourScreen(int option) {

        //0 Cambiar desde mainActivity
        //1 Cambiar desde mascotaActivity
        switch (option){
            case 0:
                onView(withId(R.id.fragment_container_main)).check(matches(isClosed(Gravity.LEFT))).perform(DrawerActions.open()); // Open Drawer
                onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_ajustes));
                break;
            case 1:
                onView(withId(R.id.fragment_container_mascota)).check(matches(isClosed(Gravity.LEFT))).perform(DrawerActions.open()); // Open Drawer
                onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_ajustes));
                break;
        }
    }
}