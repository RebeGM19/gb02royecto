package com.example.miauvet;

import android.view.Gravity;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import com.example.miauvet.ui.activities.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.containsString;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void accionMainActivity() {

        //Crea una nueva mascota
        onView(withId(R.id.floatingActionButton)).perform(click());
        onView(withId(R.id.nombre)).perform(typeText("Nombre prueba"), closeSoftKeyboard());
        onView(withId(R.id.peso)).perform(typeText("2"), closeSoftKeyboard());
        onView(withId(R.id.especie)).perform(typeText("Perro"), closeSoftKeyboard());
        onView(withId(R.id.edad)).perform(typeText("20"), closeSoftKeyboard());
        onView(withId(R.id.DesplegableRazasEditar)).perform(click());
        onData(anything()).atPosition(0).perform(click());
        onView(withId(R.id.DesplegableRazasEditar)).check(matches(withSpinnerText(containsString("Pastor alemán"))));
        onView(withId(R.id.botonGuardar)).perform(click());


        //Scroll hacia abajo para ver la nueva mascota
        RecyclerView recyclerViewMascota = activityRule.getActivity().findViewById(R.id.list);
        int itemCountMascota = recyclerViewMascota.getAdapter().getItemCount();
        onView(withId(R.id.list)).perform(RecyclerViewActions.scrollToPosition(itemCountMascota - 1));

        //Click en la nueva mascota
        onView(withId(R.id.list)).perform(RecyclerViewActions.actionOnItemAtPosition(itemCountMascota - 1, click()));
        pressBack();

        /* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        //Accedemos al navigation Drawer para ir a los recordatorios
        clickOnYourNavigationItem_ShowsYourScreen(0);

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Agregamos un recordatorio
        onView(withId(R.id.floatingActionButton)).perform(click());
        onView(withId(R.id.titulo_recordatorio_enter)).perform(typeText("Recordatorio prueba"), closeSoftKeyboard());
        onView(withId(R.id.contenido_recordatorio_enter)).perform(typeText("Rellenando recordatorio"), closeSoftKeyboard());
        onView(withId(R.id.botonGuardar)).perform(click());

        //Scroll hacia el nuevo recordatorio
        RecyclerView recyclerViewRecordatorio = activityRule.getActivity().findViewById(R.id.list_recordatorio);
        int itemCountRecordatorio = recyclerViewRecordatorio.getAdapter().getItemCount();
        onView(withId(R.id.list_recordatorio)).perform(RecyclerViewActions.scrollToPosition(itemCountRecordatorio - 1));

        //Click en el nuevo recordatorio
        onView(withId(R.id.list_recordatorio)).perform(RecyclerViewActions.actionOnItemAtPosition(itemCountRecordatorio - 1, click()));
        pressBack();

        /* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        //Accedemos al navigation Drawer para ir a los recordatorios
        clickOnYourNavigationItem_ShowsYourScreen(1);

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Agregamos una opinion
        onView(withId(R.id.opinion)).perform(typeText("Es una clinica decente"), closeSoftKeyboard());
        onView(withId(R.id.ratingBar)).perform(click());
        onView(withId(R.id.Aceptar)).perform(click());

    }


    public void clickOnYourNavigationItem_ShowsYourScreen(int option) {

        //0 Recordatorios
        //1 Opinion
        switch (option){
            case 0:
                onView(withId(R.id.fragment_container_main)).check(matches(isClosed(Gravity.LEFT))).perform(DrawerActions.open()); // Open Drawer
                onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_recordatorios));
                break;
            case 1:
                onView(withId(R.id.fragment_container_main)).check(matches(isClosed(Gravity.LEFT))).perform(DrawerActions.open()); // Open Drawer
                onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_Opiniones));
                break;
        }



    }

}
