package com.example.miauvet;

import android.widget.DatePicker;
import android.widget.TimePicker;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.contrib.PickerActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import com.example.miauvet.ui.activities.MainActivity;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.containsString;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MascotaActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void accionMascotaActivity() {

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Scroll hacia abajo para ver la nueva mascota
        RecyclerView recyclerView = mainActivityRule.getActivity().findViewById(R.id.list);
        int itemCount = recyclerView.getAdapter().getItemCount();
        onView(withId(R.id.list)).perform(RecyclerViewActions.scrollToPosition(itemCount - 1));

        //Click en la nueva mascota
        onView(withId(R.id.list)).perform(RecyclerViewActions.actionOnItemAtPosition(itemCount - 1, click()));

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Editar mascota
        onView(withId(R.id.Modificar)).perform(click());
        onView(withId(R.id.edit_nombre)).perform(clearText(), typeText("Nombre editado"), closeSoftKeyboard());
        onView(withId(R.id.edit_peso)).perform(clearText(), typeText("3"), closeSoftKeyboard());
        onView(withId(R.id.edit_especie)).perform(clearText(), typeText("Gato"), closeSoftKeyboard());
        onView(withId(R.id.edit_edad)).perform(clearText(), typeText("100"), closeSoftKeyboard());
        onView(withId(R.id.DesplegableRazasEditar)).perform(click());
        onData(anything()).atPosition(1).perform(click());
        onView(withId(R.id.DesplegableRazasEditar)).check(matches(withSpinnerText(containsString("Chihuahua"))));
        onView(withId(R.id.botonGuardar)).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Comprobamos que los datos editados se han guardado correctamente
        onView(withId(R.id.mNombre)).check(matches(withText("Nombre editado")));
        onView(withId(R.id.mPeso)).check(matches(withText("3.0")));
        onView(withId(R.id.mEspecie)).check(matches(withText("Gato")));
        onView(withId(R.id.mRaza)).check(matches(withText("Chihuahua")));
        onView(withId(R.id.mEdad)).check(matches(withText("100")));

        //Pedir consulta
        onView(withId(R.id.DesplegablePedir)).perform(click());
        onData(anything()).atPosition(1).perform(click());

        onView(withId(R.id.etPlannedDate)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 12, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.etPlannedHour)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(20, 12));
        onView(withId(android.R.id.button1)).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.BotonInsertarCita)).perform(click());


        //Pedir tratamiento
        onView(withId(R.id.DesplegablePedir)).perform(click());
        onData(anything()).atPosition(3).perform(click());

        onView(withId(R.id.elegirDiaTratamiento)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 12, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.elegirHoraTratamiento)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(20, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.DesplegableTipoTratamiento)).perform(click());
        onData(anything()).atPosition(1).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.BotonInsertarTratamiento)).perform(click());


        //Pedir servicio
        onView(withId(R.id.DesplegablePedir)).perform(click());
        onData(anything()).atPosition(2).perform(click());

        onView(withId(R.id.elegirDiaServicio)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 12, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.elegirHoraServicio)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(20, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.elegirDiaFinServicio)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 12, 14));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.elegirHoraFinServicio)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(23, 40));
        onView(withId(android.R.id.button1)).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.BotonInsertarServicio)).perform(click());


        //Borrar

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.Borrar)).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}