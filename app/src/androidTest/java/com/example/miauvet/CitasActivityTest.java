package com.example.miauvet;

import android.view.Gravity;
import android.widget.DatePicker;
import android.widget.TimePicker;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.PickerActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import com.example.miauvet.ui.activities.MainActivity;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.containsString;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class CitasActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void accionCitasActivity() {

        //Crea una nueva mascota
        onView(withId(R.id.floatingActionButton)).perform(click());
        onView(withId(R.id.nombre)).perform(typeText("Citas prueba "), closeSoftKeyboard());
        onView(withId(R.id.peso)).perform(typeText("23.2"), closeSoftKeyboard());
        onView(withId(R.id.especie)).perform(typeText("Gato"), closeSoftKeyboard());
        onView(withId(R.id.edad)).perform(typeText("10"), closeSoftKeyboard());
        onView(withId(R.id.DesplegableRazasEditar)).perform(click());
        onData(anything()).atPosition(0).perform(click());
        onView(withId(R.id.DesplegableRazasEditar)).check(matches(withSpinnerText(containsString("Pastor alemán"))));
        onView(withId(R.id.botonGuardar)).perform(click());


        //Scroll hacia abajo para ver la nueva mascota
        RecyclerView recyclerViewMascota = activityRule.getActivity().findViewById(R.id.list);
        int itemCountMascota = recyclerViewMascota.getAdapter().getItemCount();
        onView(withId(R.id.list)).perform(RecyclerViewActions.scrollToPosition(itemCountMascota - 1));

        //Click en la nueva mascota
        onView(withId(R.id.list)).perform(RecyclerViewActions.actionOnItemAtPosition(itemCountMascota - 1, click()));

        /* ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

        //Pedir consulta
        onView(withId(R.id.DesplegablePedir)).perform(click());
        onData(anything()).atPosition(1).perform(click());

        onView(withId(R.id.etPlannedDate)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 12, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.etPlannedHour)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(20, 12));
        onView(withId(android.R.id.button1)).perform(click());
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.BotonInsertarCita)).perform(click());



        //Pedir tratamiento
        onView(withId(R.id.DesplegablePedir)).perform(click());
        onData(anything()).atPosition(3).perform(click());

        onView(withId(R.id.elegirDiaTratamiento)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 12, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.elegirHoraTratamiento)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(20, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.DesplegableTipoTratamiento)).perform(click());
        onData(anything()).atPosition(1).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.BotonInsertarTratamiento)).perform(click());


        //Pedir servicio
        onView(withId(R.id.DesplegablePedir)).perform(click());
        onData(anything()).atPosition(2).perform(click());

        onView(withId(R.id.elegirDiaServicio)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 12, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.elegirHoraServicio)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(20, 12));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.elegirDiaFinServicio)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(2020, 12, 14));
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.elegirHoraFinServicio)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(23, 40));
        onView(withId(android.R.id.button1)).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.BotonInsertarServicio)).perform(click());

        /* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        //Accedemos al navigation Drawer para ir a los recordatorios
        clickOnYourNavigationItem_ShowsYourScreen();

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withText(R.string.etiqueta_tratamiento)).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withText(R.string.etiqueta_servicio)).perform(click());

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withText(R.string.etiqueta_consulta)).perform(click());

    }


    public void clickOnYourNavigationItem_ShowsYourScreen() {

        onView(withId(R.id.fragment_container_mascota)).check(matches(isClosed(Gravity.LEFT))).perform(DrawerActions.open()); // Open Drawer
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_citas));

    }

}
