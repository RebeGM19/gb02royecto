package com.example.miauvet.model;

import com.example.miauvet.database.objects.Opinion;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class OpinionTest {

    private static Opinion opinion1;
    private static Opinion opinion2;

    @BeforeClass
    public static void initClass(){
        opinion1 = new Opinion();
        opinion1.setId(1);
        opinion1.setTitulo("Buena clinica");
        opinion1.setPuntuacion(5.0f);
    }

    @Test
    public void testOpinionNotNull(){
        assertNotNull(opinion1);
    }

    @Test
    public void testOpinionNull(){
        assertNull(opinion2);
    }

    @Test
    public void testIdOpinion(){
        assertEquals(opinion1.getId(), 1);
    }

    @Test
    public void testNombreOpinion(){
        assertEquals(opinion1.getTitulo(), "Buena clinica");
    }

    @Test
    public void testPuntuacionOpinion(){
        assertEquals((double) opinion1.getPuntuacion(), 5.0f, 0);
    }

    @AfterClass
    public static void fin(){
        opinion1 = null;
    }

}
