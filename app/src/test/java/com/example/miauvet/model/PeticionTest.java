package com.example.miauvet.model;

import com.example.miauvet.database.objects.Peticion;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class PeticionTest {

    private static Peticion peticion1;
    private static Peticion peticion2;

    @BeforeClass
    public static void initClass() {
        peticion1 = new Peticion();
        peticion1.setId(1);
        peticion1.setTipo(2);
        peticion1.setMascota(3);
        peticion1.setDia(new Date(1999, 12, 12)); // Deprecado por temas de internacionalizacion
        peticion1.setDiafin(new Date(2001, 1, 1));
        peticion1.setTipotratamiento(2);
    }

    @Test
    public void testPeticionNotNull(){
        assertNotNull(peticion1);
    }

    @Test
    public void testPeticionNull(){
        assertNull(peticion2);
    }

    @Test
    public void testTipoPeticion(){
        assertEquals((long) peticion1.getTipo(), 2);
    }

    @Test
    public void testMascotaPeticion(){
        assertEquals(peticion1.getMascota(), 3);
    }

    @Test
    public void testDiaPeticion(){
        Date fecha = new Date(1999, 12, 12);
        assertEquals(peticion1.getDia(), fecha);
    }

    @Test
    public void testDiaFinPeticion(){
        Date fecha = new Date(2001, 1, 1);
        assertEquals(peticion1.getDiafin(), fecha);
    }

    @AfterClass
    public static void fin(){
        peticion1 = null;
    }

}
