package com.example.miauvet.model;

import com.example.miauvet.database.objects.Recordatorio;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class RecordatorioTest {

    private static Recordatorio recordatorio1;
    private static Recordatorio recordatorio2;

    @BeforeClass
    public static void initClass() {
        recordatorio1 = new Recordatorio();
        recordatorio1.setId(1);
        recordatorio1.setTitulo("La lista de la compra");
        recordatorio1.setContenido("Dos de cal y una de arena");
    }

    @Test
    public void testRecordatorioNotNull(){
        assertNotNull(recordatorio1);
    }

    @Test
    public void testRecordatorioNull(){
        assertNull(recordatorio2);
    }

    @Test
    public void testIdRecordatorio(){
        assertEquals(recordatorio1.getId(), 1);
    }

    @Test
    public void testTituloRecordatorio(){
        assertEquals(recordatorio1.getTitulo(), "La lista de la compra");
    }

    @AfterClass
    public static void fin(){
        recordatorio1 = null;
    }

}
