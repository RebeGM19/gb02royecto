package com.example.miauvet.model;

import com.example.miauvet.database.objects.Mascota;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MascotaTest {

    private static Mascota mascota1;
    private static Mascota mascota2;

    @BeforeClass
    public static void initClass(){
        mascota1 = new Mascota();
        mascota1.setId(1);
        mascota1.setNombre_mascota("Torta");
        mascota1.setPeso(99.9f);
        mascota1.setEdad(20);
    }

    @Test
    public void testMascotaNotNull(){
        assertNotNull(mascota1);
    }

    @Test
    public void testMascotaNull(){
        assertNull(mascota2);
    }

    @Test
    public void testIdMascota(){
        assertEquals(mascota1.getId(), 1);
    }

    @Test
    public void testNombreMascota(){
        assertEquals(mascota1.getNombre_mascota(), "Torta");
    }

    @Test
    public void testPesoMascota(){
        assertEquals((double) mascota1.getPeso(), 99.9f, 0);
    }

    @Test
    public void testEdadMascota(){
        assertEquals(mascota1.getEdad(), 20);
    }

    @AfterClass
    public static void fin(){
        mascota1 = null;
    }

}
