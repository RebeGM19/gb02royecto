package com.example.miauvet.room;

import com.example.miauvet.database.objects.Peticion;
import com.example.miauvet.database.roomBD.PeticionDAO;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class PeticionDAOTest {

    @Mock
    PeticionDAO peticionDAO;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void daoNotNull(){
        assertNotNull(peticionDAO);
    }

    @Test
    public void listaPeticions(){
        List<Peticion> peticiones = new ArrayList<>();
        List<Peticion> peticionesNull = new ArrayList<>();
        peticiones.add(crearPeticion(3));
        peticiones.add(crearPeticion(6));

        when(peticionDAO.getSelectAll()).thenReturn(peticiones);

        assertEquals(peticionDAO.getSelectAll(), peticiones);
        assertNotEquals(peticionDAO.getSelectAll(), peticionesNull);
        assertFalse(peticionDAO.getSelectAll().isEmpty());
        assertTrue(peticionDAO.getSelectAll().size() != 310);
    }

    @Test
    public void insertarPeticion(){
        Peticion Peticion = crearPeticion(5);
        when(peticionDAO.insert(Peticion)).thenReturn((long) 0);
        assertNotNull(peticionDAO.insert(Peticion));
    }

    private Peticion crearPeticion(Integer id){
        Peticion peticion = new Peticion();
        peticion.setId((long) id);
        peticion.setTipo(3);
        peticion.setTipotratamiento(1);
        peticion.setDia(new Date(2020, 12, 12));
        peticion.setDiafin(new Date(2021, 1, 1));
        peticion.setMascota(1);
        return peticion;
    }
    
}
