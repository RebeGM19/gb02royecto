package com.example.miauvet.room;

import com.example.miauvet.database.objects.Opinion;
import com.example.miauvet.database.roomBD.OpinionDAO;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class OpinionDAOTest {

    @Mock
    OpinionDAO opinionDAO;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void daoNotNull(){
        assertNotNull(opinionDAO);
    }

    @Test
    public void listaOpinions(){
        List<Opinion> opiniones = new ArrayList<>();
        opiniones.add(crearOpinion(3));
        opiniones.add(crearOpinion(4));

        when(opinionDAO.getSelectAll()).thenReturn(opiniones);

        assertEquals(opinionDAO.getSelectAll(), opiniones);
        assertNotEquals(opinionDAO.getSelectAll(), null);
        assertFalse(opinionDAO.getSelectAll().isEmpty());
        assertTrue(opinionDAO.getSelectAll().size() != 5);
    }

    @Test
    public void insertarOpinion(){
        Opinion Opinion = crearOpinion(2);
        when(opinionDAO.insert(Opinion)).thenReturn((long) 0);
        assertNotNull(opinionDAO.insert(Opinion));
    }

    private Opinion crearOpinion(Integer id){
        Opinion opinion = new Opinion();
        opinion.setId((long) id);
        opinion.setPuntuacion(5.0f);
        opinion.setTitulo("Muy buen trato, aunque gasto mucha gasolina");
        return opinion;
    }

}
