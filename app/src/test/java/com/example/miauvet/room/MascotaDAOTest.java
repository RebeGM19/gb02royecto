package com.example.miauvet.room;

import com.example.miauvet.database.objects.Mascota;
import com.example.miauvet.database.roomBD.MascotaDAO;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class MascotaDAOTest {

    @Mock
    MascotaDAO mascotaDAO;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void daoNotNull(){
        assertNotNull(mascotaDAO);
    }

    @Test
    public void listaMascotas(){
        List<Mascota> mascotas = new ArrayList<>();
        mascotas.add(crearMascota(0));
        mascotas.add(crearMascota(1));

        when(mascotaDAO.getSelectAll()).thenReturn(mascotas);

        assertEquals(mascotaDAO.getSelectAll(), mascotas);
        assertNotEquals(mascotaDAO.getSelectAll(), null);
        assertFalse(mascotaDAO.getSelectAll().isEmpty());
        assertTrue(mascotaDAO.getSelectAll().size() == 2);
    }

    @Test
    public void insertarMascota(){
        Mascota mascota = crearMascota(3);
        when(mascotaDAO.insert(mascota)).thenReturn((long) 0);
        assertNotNull(mascotaDAO.insert(mascota));
    }

    @Test
    public void actualizarMascota(){
        Mascota mascota = crearMascota(3);
        mascota.setNombre_mascota("HugoBass");
        //Cuando actualiza, siempre se cambia una tupla
        when(mascotaDAO.updateStatus(mascota)).thenReturn(1);
        assertEquals(mascotaDAO.updateStatus(mascota), 1);
        assertNotEquals(mascotaDAO.updateStatus(mascota), 2);
    }

    @Test
    public void borrarMascota(){
        Mascota mascota1 = crearMascota(3);
        when(mascotaDAO.borrarMascotaPorId((long) 3)).thenReturn(1);
        //Si ha encontrado la mascota a borrar, devuelve 1 porque al ser clave primaria solo afecta a 1 tupla
        //Si no encuentra la mascota a borrar, devuelve 0
        assertEquals(mascotaDAO.borrarMascotaPorId(3), 1);
        assertNotEquals(mascotaDAO.borrarMascotaPorId(3), 0);
    }

    @Test
    public void selectMascota(){
        long id = (long) 6;
        when(mascotaDAO.selectMascota(id)).thenReturn(buscarMascota(6));
        assertNull(mascotaDAO.selectMascota(3));
        assertNotNull(mascotaDAO.selectMascota(6));
    }


    private Mascota crearMascota(Integer id){
        Mascota mascota = new Mascota();
        mascota.setId((long) id);
        mascota.setNombre_mascota("Jabi");
        mascota.setDuenio("Belen Esteban");
        mascota.setPeso(20.0f);
        mascota.setEdad(2);
        mascota.setEspecie("Perro");
        mascota.setRaza("Chihuahua");
        return mascota;
    }

    private Mascota buscarMascota(long id) {
        Mascota resultado = null;
        List<Mascota> listado = new ArrayList<>();
        Mascota mascota1 = crearMascota(5);
        Mascota mascota2 = crearMascota(6);
        Mascota mascota3 = crearMascota(7);
        listado.add(mascota1);
        listado.add(mascota2);
        listado.add(mascota3);
        for (int i = 0; i < listado.size(); i++){

            if (listado.get(i).getId() == id)
                resultado = listado.get(i);
        }
        return resultado;
    }

}
