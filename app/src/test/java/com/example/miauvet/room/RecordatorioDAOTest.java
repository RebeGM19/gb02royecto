package com.example.miauvet.room;

import com.example.miauvet.database.objects.Recordatorio;
import com.example.miauvet.database.roomBD.RecordatorioDAO;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class RecordatorioDAOTest {

    @Mock
    RecordatorioDAO recordatorioDAO;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void daoNotNull(){
        assertNotNull(recordatorioDAO);
    }

    @Test
    public void listaRecordatorios(){
        List<Recordatorio> recordatorios = new ArrayList<>();
        recordatorios.add(crearRecordatorio(3));
        recordatorios.add(crearRecordatorio(6));

        when(recordatorioDAO.getSelectAll()).thenReturn(recordatorios);

        assertEquals(recordatorioDAO.getSelectAll(), recordatorios);
        assertNotEquals(recordatorioDAO.getSelectAll(), null);
        assertTrue(!recordatorioDAO.getSelectAll().isEmpty());
        assertFalse(recordatorioDAO.getSelectAll().size() == 20);
    }

    @Test
    public void insertarRecordatorio(){
        Recordatorio Recordatorio = crearRecordatorio(5);
        when(recordatorioDAO.insert(Recordatorio)).thenReturn((long) 0);
        assertNotNull(recordatorioDAO.insert(Recordatorio));
    }

    private Recordatorio crearRecordatorio(Integer id){
        Recordatorio recordatorio = new Recordatorio();
        recordatorio.setId((long) id);
        recordatorio.setTitulo("Compra pan");
        recordatorio.setContenido("Manu, ya he comprado pan");
        return recordatorio;
    }
    
}
