package com.example.miauvet.network;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.example.miauvet.database.network.MiauNetworkData;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class APIMascotasTest {

    private static MiauNetworkData apiMascotas;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @BeforeClass
    public static void initClass(){
        apiMascotas = new MiauNetworkData();
    }

    @Test
    public void instanciaNotNull(){
        assertNotNull(apiMascotas);
    }

    @Test
    public void leerMascotas(){
        assertNotNull(apiMascotas.conseguirMascotas());
        assertTrue(apiMascotas.conseguirMascotas().getValue().size()==14);
    }

}
