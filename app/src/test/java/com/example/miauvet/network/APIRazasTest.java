package com.example.miauvet.network;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.example.miauvet.database.network.MiauNetworkData;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class APIRazasTest {

    private static MiauNetworkData apiMascotas;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @BeforeClass
    public static void initClass(){
        apiMascotas = new MiauNetworkData();
    }

    @Test
    public void instanciaNotNull(){
        assertNotNull(apiMascotas);
    }

    @Test
    public void leerRazas(){
        assertNotNull(apiMascotas.conseguirRazas());
        assertFalse(apiMascotas.conseguirRazas().size()!=10);
    }

}
