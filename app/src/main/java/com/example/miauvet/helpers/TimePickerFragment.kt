package com.example.miauvet.helpers

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import java.util.*

class TimePickerFragment : DialogFragment() {

    private var listener: TimePickerDialog.OnTimeSetListener? = null

    /*
    *metodo que permite definir un alertDialog personalizado, en este caso el contenido seran horas y minutos
    * @params: savedInstanceState de tipo Bundle
    * @return: Dialog*/
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)

        return TimePickerDialog(context!!, listener, hour, minute, true)
    }

    /*metodo que crea el fragmento donde se mostrara el AlertDialog
    * @params: listener de tipo TimePickerDialog.OnTimeSetListener
    * @return: TimePickerFragment*/
    companion object {
        fun newInstance(listener: TimePickerDialog.OnTimeSetListener?): TimePickerFragment {
            val fragment = TimePickerFragment()
            fragment.listener = listener
            return fragment
        }
    }
}