package com.example.miauvet.helpers

import android.app.AlertDialog
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.miauvet.R
import com.example.miauvet.database.objects.Peticion
import java.util.*

class CitasAdapter(var context: Context?, var list: List<Peticion>)
    : RecyclerView.Adapter<CitasViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitasViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(parent.context)
        return CitasViewHolder(context, inflater, parent)
    }

    override fun onBindViewHolder(holder: CitasViewHolder, position: Int) {
        val cita: Peticion = list[position]
        holder.bind(cita)
    }

    override fun getItemCount(): Int = list.size

}

class CitasViewHolder(context: Context, inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_cita, parent, false)), View.OnClickListener {

    private val mCitaView: TextView?
    private var idCita: String
    private var tipo: Int?
    private var mascota: Long
    private var dia: Date?
    private var diaFin: Date?
    private var tipoTratamiento: Int?
    var mContext = context

    init {
        mCitaView = itemView.findViewById(R.id.lista_citas)
        idCita = "0"
        tipo = null
        mascota = -1
        dia = null
        diaFin = null
        tipoTratamiento = null
        itemView.setOnClickListener(this)
    }

    fun bind(cita: Peticion) {
        idCita = cita.id.toString()
        mCitaView?.text = cita.dia.toString()
        tipo = cita.tipo
        mascota = cita.mascota
        dia = cita.dia
        diaFin = cita.diafin
        tipoTratamiento = cita.tipotratamiento
    }

    override fun onClick(v: View?) {
        val alertDialog = AlertDialog.Builder(mContext)
        when(tipo){
            1 -> {
                alertDialog.setMessage("Consulta día " + dia.toString())
            }
            2 -> {
                var tratamiento = ""
                when(tipoTratamiento){
                    1 -> tratamiento = "Baño"
                    2 -> tratamiento = "Cortar pelo"
                    3 -> tratamiento = "Cortar uñas"
                }
                alertDialog.setMessage("Tratamiento día " + dia.toString() + "\nde " + tratamiento)
            }
            3 -> {
                alertDialog.setMessage("Servicio desde " + dia.toString() + " hasta " + diaFin.toString())
            }
        }
        alertDialog.setNeutralButton("Ok") { _, _ -> }
        alertDialog.show()
    }

}

