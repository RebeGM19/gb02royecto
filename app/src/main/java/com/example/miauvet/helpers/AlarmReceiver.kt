package com.example.miauvet.helpers

import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import com.example.miauvet.database.MiauRepository
import com.example.miauvet.database.network.MiauNetworkData

class AlarmReceiver : BroadcastReceiver() {
    lateinit var repository: MiauRepository
    lateinit var networkData: MiauNetworkData

    override fun onReceive(context: Context, intent: Intent) {
        networkData = MiauNetworkData.getInstance(context)!!
        repository = MiauRepository.getInstance(context, networkData)!!
        repository.selectMascotasDeApi()
        repository.cargarRazasDeApi()
        repository.leerMascotas()
    }
}