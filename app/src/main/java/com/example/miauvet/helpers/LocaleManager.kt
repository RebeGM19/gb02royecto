@file:Suppress("DEPRECATION", "NAME_SHADOWING")

package com.example.miauvet.helpers

import android.content.Context
import android.content.res.Configuration
import android.preference.PreferenceManager
import androidx.annotation.StringDef
import java.util.*

object LocaleManager {

    internal const val ENGLISH = "en"
    internal const val SPANISH = "es"
    private val LANGUAGE_KEY = "language_key"
    var currentLanguage = "es"

    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    @StringDef(
        ENGLISH,
        SPANISH
    )
    annotation class LocaleDef {
        companion object {
            val SUPPORTED_LOCALES = arrayOf(
                ENGLISH,
                SPANISH
            )
        }
    }

    fun setLocale(mContext: Context): Context {
        return updateResources(
            mContext,
            getLanguagePref(mContext)
        )
    }

    fun setNewLocale(mContext: Context, @LocaleDef language: String): Context {
        setLanguagePref(mContext, language)
        currentLanguage = language
        return updateResources(mContext, language)
    }

    fun getLanguagePref(mContext: Context): String {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        return mPreferences.getString(
            LANGUAGE_KEY,
            SPANISH
        ).toString()
    }

    private fun setLanguagePref(mContext: Context, localeKey: String) {
        val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
        mPreferences.edit().putString(LANGUAGE_KEY, localeKey).apply()
    }

    private fun updateResources(context: Context, language: String): Context {
        var context = context
        val locale = Locale(language)
        Locale.setDefault(locale)

        val res = context.resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)

        context = context.createConfigurationContext(config)
        return context
    }
}