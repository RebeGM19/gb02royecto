package com.example.miauvet.helpers

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.miauvet.R
import com.example.miauvet.database.objects.Recordatorio

class RecordatoriosAdapter (var context: Context?, var list: List<Recordatorio>)
    : RecyclerView.Adapter<RecordatorioViewHolder>() {

    /*
   * metodo que se llama cuando RecyclerView necesita una nueva RecyclerView.ViewHolder del tipo dado para representar un elemento.
   * @params: parent de tipo ViewGroup y viewType de tipo Int
   * @return: RecordatorioViewHolder*/
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecordatorioViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(parent.context)
        return RecordatorioViewHolder(context, inflater, parent)
    }

    /*
   *metodo llamado por RecyclerView para mostrar los datos en la posición especificada. Este método debe actualizar
   * el contenido del itemView para reflejar el elemento en la posición dada.
   * @params: holder de tipo RecordatorioHolder y position de tipo Int
   * */
    override fun onBindViewHolder(holder: RecordatorioViewHolder, position: Int) {
        val recordatorio: Recordatorio = list[position]
        holder.bind(recordatorio)
    }

    /*
    *metodo que devuelve el número total de elementos en el adaptador.
    * @return: Int*/
    override fun getItemCount(): Int = list.size

}

class RecordatorioViewHolder(context: Context, inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_recordatorio, parent, false)), View.OnClickListener {

    private val mNombreView: TextView?
    private var tituloRecordatorio: String
    private var contenidoRecordatorio: String
    var mContext = context

    init {
        mNombreView = itemView.findViewById(R.id.lista_recordatorios)
        tituloRecordatorio = ""
        contenidoRecordatorio = ""
        itemView.setOnClickListener(this)
    }

    /*
    *método que es llamado internamente por onBindViewHolder(ViewHolder, int)para actualizar los elementos del RecyclerView en la posicion correspondiente
    * @params: recordatorio de tipo Recordatorio
    * */
    fun bind(recordatorio: Recordatorio) {
        tituloRecordatorio = recordatorio.titulo.toString()
        contenidoRecordatorio = recordatorio.contenido.toString()
        mNombreView?.text = recordatorio.titulo
    }

    /*
    * metodo que describe el comportamiento cuando se haga click en alguno de los elementos del RecyclerView
    * @params: v de tipo View
    * */
    override fun onClick(v: View?) {
        val alertDialog = AlertDialog.Builder(mContext)
        alertDialog.setMessage("Titulo: " + tituloRecordatorio + "\nContenido: " + contenidoRecordatorio)
        alertDialog.setNeutralButton("Ok") { _, _ -> }
        alertDialog.show()
    }

}