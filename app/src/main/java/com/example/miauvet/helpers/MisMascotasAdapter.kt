package com.example.miauvet.helpers

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.startActivity
import com.example.miauvet.R
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.ui.activities.MascotaActivity

class MisMascotasAdapter(var context: Context?, var list: List<Mascota>)
    : RecyclerView.Adapter<MascotaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MascotaViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(parent.context)
        return MascotaViewHolder(context, inflater, parent)
    }

    override fun onBindViewHolder(holder: MascotaViewHolder, position: Int) {
        val mascota: Mascota = list[position]
        holder.bind(mascota)
    }

    override fun getItemCount(): Int = list.size

}

class MascotaViewHolder(context: Context, inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_mascota, parent, false)), View.OnClickListener {

    private val CardView_mascotas: CardView?
    private val Nombre_mascotas: TextView?
    private val Foto_mascotas: ImageView
    private var idMascota: String
    var mContext = context

    init {
        CardView_mascotas = itemView.findViewById(R.id.cv_mascotas)
        Nombre_mascotas = itemView.findViewById(R.id.cv_mascota_nombre)
        Foto_mascotas = itemView.findViewById(R.id.cv_mascota_foto)
        idMascota = "0"
        itemView.setOnClickListener(this)
    }

    fun bind(mascota: Mascota) {
        idMascota = mascota.id.toString()
        Nombre_mascotas?.text = mascota.nombre_mascota
        Foto_mascotas.setImageResource(R.drawable.ic_perro)
    }

    override fun onClick(v: View?) {
        val intentMascota = Intent(mContext, MascotaActivity::class.java)
        intentMascota.putExtra("Mascota", idMascota) // Mandar un intent con la ID de la mascota para luego buscarla en la BD desde MascotaActivity
        startActivity(mContext, intentMascota, null)
    }

}