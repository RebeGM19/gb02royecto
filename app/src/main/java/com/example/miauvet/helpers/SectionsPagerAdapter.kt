package com.example.miauvet.helpers

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.miauvet.R
import com.example.miauvet.ui.fragments.MostrarConsultasFragment
import com.example.miauvet.ui.fragments.MostrarServiciosFragment
import com.example.miauvet.ui.fragments.MostrarTratamientosFragment


private val TAB_TITLES = arrayOf(
    R.string.etiqueta_consulta,
    R.string.etiqueta_tratamiento,
    R.string.etiqueta_servicio
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context?, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    var mostrarConsultasFragment: MostrarConsultasFragment = MostrarConsultasFragment.newInstance()
    var mostrarTratamientosFragment: MostrarTratamientosFragment = MostrarTratamientosFragment.newInstance()
    var mostrarServiciosFragment: MostrarServiciosFragment = MostrarServiciosFragment.newInstance()

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when(position){
            0 -> {
                fragment = mostrarConsultasFragment
            }
            1 -> {
                fragment = mostrarTratamientosFragment
            }
            2 -> {
                fragment = mostrarServiciosFragment
            }
        }
        return fragment!!
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context?.resources?.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 3 total pages.
        return 3
    }
}