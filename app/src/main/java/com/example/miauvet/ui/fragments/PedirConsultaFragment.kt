package com.example.miauvet.ui.fragments

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.miauvet.R
import com.example.miauvet.database.objects.Peticion
import com.example.miauvet.helpers.DatePickerFragment
import com.example.miauvet.helpers.TimePickerFragment
import com.example.miauvet.ui.activities.MascotaActivity

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class PedirConsultaFragment : Fragment() {

    private var param1: String? = null
    private var param2: String? = null
    var selectedDate: String = "00/00/0000"
    var selectedHour: String = "00:00"

    //private var listener: OnFragmentInteractionListener? = null
    companion object {
        @JvmStatic
        fun newInstance(param1: String?, param2: String?) =
            PedirConsultaFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    /*
    *metodo que crea la activity con todos los elementos correspondientes
    * @params: savedInstanceState de tipo Bundle
    * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    /*metodo llamado para que el fragmento instancie su vista de interfaz de usuario
   * @params: inflater de tipo LayoutInflater, container de tipo ViewGroup y savedInstanceState de tipo Bundle
   * @return: View */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pedir_consulta, container, false)
    }

    /*metodo que se llama cuando se ha creado la actividad del fragmento
   * @params: savedInstanceState de tipo Bundle*/
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //boton para seleccionar una fecha
        val datePickerButton = view?.findViewById(R.id.etPlannedDate) as EditText
        datePickerButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                showDatePickerDialog(datePickerButton)
            }
        })

        //boton para seleccionar una hora
        val timePickerButton = view?.findViewById(R.id.etPlannedHour) as EditText
        timePickerButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                showTimePickerDialog(timePickerButton)
            }
        })

        val mascotaActivity = activity as MascotaActivity
        val aniadirCitaButton = view?.findViewById(R.id.BotonInsertarCita) as Button

        //se guarda la cita
        aniadirCitaButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val timestamp = "$selectedDate $selectedHour"
                val peticion = Peticion(
                    0,
                    1,
                    mascotaActivity.mascotaRecibida.toLong(),
                    timestamp,
                    null,
                    null)
                mascotaActivity.mascotaVM.guardarPeticion(peticion)
                Toast.makeText(activity, "Cita guardada $timestamp", Toast.LENGTH_SHORT).show()
                mascotaActivity.supportFragmentManager.popBackStack()
            }
        })

    }

    fun formatearFecha(anio: Int, mes: Int, dia: Int): String{
        var mesAux: String = mes.toString()
        var diaAux: String = dia.toString()
        if(mes < 10){
            mesAux = "0$mes"
        }
        if(dia < 10){
            diaAux = "0$dia"
        }
        return "$anio-$mesAux-$diaAux"
    }

    fun formatearHora(hora: Int, minuto: Int): String{
        var minutoAux: String = minuto.toString()
        var horaAux: String = hora.toString()
        if(minuto < 10){
            minutoAux = "0$minuto"
        }
        if(hora < 10){
            horaAux = "0$hora"
        }
        return "$horaAux:$minutoAux:00"
    }

    fun showDatePickerDialog(datePickerButton: EditText) {
        val newFragment = DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener { _, year, month, day ->
            // +1 porque Enero es 0
            selectedDate = formatearFecha(year, month+1, day)
            datePickerButton.setText(selectedDate)
        })
        fragmentManager?.let { newFragment.show(it, "datePicker") }
    }

    fun showTimePickerDialog(timePickerButton: EditText) {
        val newFragment = TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            selectedHour = formatearHora(hour, minute)
            timePickerButton.setText(selectedHour)
        })
        fragmentManager?.let { newFragment.show(it, "datePicker") }
    }


}
