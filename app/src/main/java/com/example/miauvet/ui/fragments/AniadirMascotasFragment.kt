package com.example.miauvet.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.R
import com.example.miauvet.ui.activities.MainActivity

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

@Suppress("NAME_SHADOWING")
class AniadirMascotasFragment : Fragment() {

    lateinit var mascota1: Mascota
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var mNombre: EditText
    private lateinit var mPeso: EditText
    private lateinit var mEspecie: EditText
    private lateinit var mEdad: EditText
    private var mDuenio: String = "Belen Esteban"

    companion object {
        @JvmStatic
        fun newInstance(param1: String?, param2: String?) =
            AniadirMascotasFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_aniadir_mascotas, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mainActivity = activity as MainActivity    // Hacer invisible boton flotante
        mainActivity.getFAB().visibility = View.INVISIBLE

        mNombre = view?.findViewById(R.id.nombre)!!
        mPeso = view?.findViewById(R.id.peso)!!
        mEdad = view?.findViewById(R.id.edad)!!
        var raza = "Error" // Se cambia a través del spinner
        mEspecie = view?.findViewById(R.id.especie)!!

        /* Spinner */
        val spinner = view?.findViewById(R.id.DesplegableRazasEditar) as Spinner

        val adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, mainActivity.razas) // Meter la lista de razas en el adaptador
        spinner.adapter = adapter

        /* Listener del spinner */
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                if (view != null && pos in 0..9) { // Porque si no la API peta, Comprobamos el rango
                    onButtonPressed(pos) // Array empieza en 0
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            private fun onButtonPressed(i: Int) {
                raza = mainActivity.razas.get(i) // Coger la raza de la pos i de la lista
            }
        }

        /* ---------------------- */

        /* Boton para guardar la Mascota */

        val submitButton = view?.findViewById(R.id.botonGuardar) as Button
        submitButton.setOnClickListener(object: View.OnClickListener {
            override fun onClick (v: View?) {

                val nombre = mNombre.text.toString()
                val peso = mPeso.text.toString().toFloat()
                val especie = mEspecie.text.toString()
                val edad = mEdad.text.toString().toInt()

                // Crear objeto de tipo Mascota para pasar por parametro
                mascota1 =
                    Mascota(
                        0,
                        nombre,
                        mDuenio,
                        peso,
                        especie,
                        raza,
                        edad
                    )

                val mainActivity = activity as MainActivity
                mainActivity.mainVM.guardarMascota(mascota1)
                mainActivity.supportFragmentManager.popBackStack() // Volvemos a la pantalla anterior
                Toast.makeText(activity, "Mascota añadida", Toast.LENGTH_SHORT).show()
            }
        })

        /* ---------------------------- */
    }

}
