package com.example.miauvet.ui.activities

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.preference.PreferenceManager
import androidx.preference.PreferenceFragmentCompat
import com.example.miauvet.helpers.LocaleManager
import com.example.miauvet.R


class SettingsActivity : BaseActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    /*
    * Metodo que crea la activity con todos los elementos correspondientes
    * @params: savedInstanceState de tipo Bundle
    * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val ab = supportActionBar

        // Habilitar navegacion ancestral
        ab?.setDisplayHomeAsUpEnabled(true)
        ab?.setTitle(R.string.titulo_ajustes)

        supportFragmentManager.beginTransaction().add(android.R.id.content,
            SettingsFragment()
        ).commit()

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        // Registra una devolución de llamada para ser invocada cuando ocurre un cambio en una preferencia.
        sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    class SettingsFragment: PreferenceFragmentCompat(){
       /*
       * Metodo que proporciona las preferencias
       * @params: savedInstanceState de tipo Bundle y rootKey de tipo String
       */
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
           //cambia el xml actual por el de preferences
            setPreferencesFromResource(R.xml.preferences, rootKey)
        }
    }

    /*
    * Metodo que permite el cambio de idioma
    * @params: mContext de tipo AppCompatActivity y @LocaleManager.LocaleDef language de tipo String
    */
    private fun setNewLocale(mContext: AppCompatActivity, @LocaleManager.LocaleDef language: String) {
        LocaleManager.setNewLocale(mContext, language)
        finish()
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    /*
    * Metodo que destruye la activity
    */
    override fun onDestroy() {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        //anula el registro de una devolución de llamada anterior.
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
        super.onDestroy()
    }

    /* Metodo que muestra las distintas opciones de ajustes
    * @params: sharedPreferences de tipo SharedPreferences y key de tipo String
    */
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        when(key){
            "language_preference"->{
                if(sharedPreferences.getString(key, "en") == "en"){
                    Toast.makeText(this, "Inglés", Toast.LENGTH_SHORT).show()
                    setNewLocale(this, LocaleManager.ENGLISH)
                }else if(sharedPreferences.getString(key, "es") == "es"){
                    Toast.makeText(this, "Español", Toast.LENGTH_SHORT).show()
                    setNewLocale(this, LocaleManager.SPANISH)
                }
            }
        }
    }

}
