package com.example.miauvet.ui.activities

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import com.example.miauvet.R
import com.example.miauvet.database.MiauRepository
import com.example.miauvet.database.network.MiauNetworkData
import com.example.miauvet.database.objects.Peticion
import com.example.miauvet.ui.fragments.MostrarConsultasFragment
import com.example.miauvet.ui.fragments.MostrarServiciosFragment
import com.example.miauvet.ui.fragments.MostrarTratamientosFragment
import com.example.miauvet.ui.viewmodel.CitasViewModel
import com.example.miauvet.ui.viewmodel.CitasViewModelFactory

class CitasActivity : BaseActivity(), MostrarConsultasFragment.OnListFragmentInteractionListener, MostrarTratamientosFragment.OnListFragmentInteractionListener, MostrarServiciosFragment.OnListFragmentInteractionListener {
    lateinit var citasVM: CitasViewModel
    var peticioncitas: MutableLiveData<ArrayList<Peticion>> = MutableLiveData<ArrayList<Peticion>>()

    override fun onListFragmentInteraction(pos: Int?) {
        /*Se necesita el método para que pueda extender de los ListFragmentInteractionListener
        * de cada Fragment con los que esta Activity va a trabajar.
        * Como no es necesario sobreescribir el método, se queda en blanco*/
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_citas)
        configureNavigationDrawer(R.id.fragment_container_citas)

        networkData = MiauNetworkData.getInstance(this)!!
        repositorio = MiauRepository.getInstance(this, networkData)!!

        citasVM = ViewModelProviders.of(this, CitasViewModelFactory(this)).get(CitasViewModel::class.java)
    }

    fun construirLiveDataPeticiones(peticiones: ArrayList<Peticion>){
        peticioncitas.postValue(peticiones)
    }

}