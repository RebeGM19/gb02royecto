package com.example.miauvet.ui.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.miauvet.database.MiauRepository
import com.example.miauvet.database.network.MiauNetworkData
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.database.objects.Recordatorio

class MainViewModel(context: Context) : ViewModel() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var repository: MiauRepository
        private var instancia: MainViewModel? = null
        fun getInstance(context: Context): MainViewModel? {
            if (instancia == null) {
                instancia = MainViewModel(context)
                repository = MiauRepository.getInstance(context, MiauNetworkData.getInstance(context)!!)!!
            }
            return instancia
        }
    }

    fun cargaInicial(){
        repository.selectMascotasDeApi()
    }

    fun cargaRazas(){
        repository.cargarRazasDeApi()
    }

    fun leerMascotas(){
        repository.leerMascotas()
    }

    fun guardarMascota(mascota: Mascota){
        repository.insertarMascota(mascota)
    }

    fun leerRecordatorios(){
        repository.listarRecordatorios()
    }

    fun guardarRecordatorio(recordatorio: Recordatorio){
        repository.insertarRecordatorio(recordatorio)
    }



}