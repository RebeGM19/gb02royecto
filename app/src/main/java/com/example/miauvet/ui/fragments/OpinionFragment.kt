package com.example.miauvet.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.miauvet.R
import com.example.miauvet.database.objects.Opinion
import com.example.miauvet.ui.activities.MainActivity
import com.google.android.material.tabs.TabLayout

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class OpinionFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    companion object {
        @JvmStatic
        fun newInstance(param1: String?, param2: String?) =
            OpinionFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    /*
    *metodo que crea la activity con todos los elementos correspondientes
    * @params: savedInstanceState de tipo Bundle
    * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    /*metodo llamado para que el fragmento instancie su vista de interfaz de usuario
   * @params: inflater de tipo LayoutInflater, container de tipo ViewGroup y savedInstanceState de tipo Bundle
   * @return: View */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_opinion, container, false)
    }

    /*metodo que se llama cuando se ha creado la actividad del fragmento
    * @params: savedInstanceState de tipo Bundle*/
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val tabs = view?.findViewById(R.id.tabs) as TabLayout?
        tabs?.visibility = View.INVISIBLE

        val rBar = view?.findViewById(R.id.ratingBar) as RatingBar
        val opinion = view?.findViewById(R.id.opinion) as EditText
        val submitOpinion = view?.findViewById(R.id.Aceptar) as Button
        submitOpinion.setOnClickListener (object: View.OnClickListener {
            //se guarda la opinion
            override fun onClick(v: View?) {
                val myOpinion = Opinion(0, opinion.text.toString(), rBar.rating)
                val mainActivity = activity as MainActivity
                mainActivity.guardarOpinion(myOpinion)
                Toast.makeText(activity, "Opinion guardada: " + rBar.rating.toString(), Toast.LENGTH_SHORT).show()
                mainActivity.supportFragmentManager.popBackStack()
            }
        })
    }

}



