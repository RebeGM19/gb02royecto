package com.example.miauvet.ui.activities

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import com.example.miauvet.helpers.LocaleManager
import com.example.miauvet.R
import com.example.miauvet.database.MiauRepository
import com.example.miauvet.database.objects.Recordatorio
import com.example.miauvet.database.network.MiauNetworkData
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.helpers.AlarmReceiver
import com.example.miauvet.ui.fragments.*
import com.example.miauvet.ui.viewmodel.MainViewModel
import com.example.miauvet.ui.viewmodel.MainViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton

open class MainActivity : BaseActivity(), MisMascotasFragment.OnListFragmentInteractionListener, RecordatorioFragment.OnListFragmentInteractionListener{

    var tipo: String? = null
    lateinit var mainVM: MainViewModel
    var mascotitas: MutableLiveData<ArrayList<Mascota>> = MutableLiveData()
    var recordatorcitos: MutableLiveData<ArrayList<Recordatorio>> = MutableLiveData()

    private var alarmMgr: AlarmManager? = null
    private lateinit var alarmIntent: Intent
    private lateinit var pendingIntent: PendingIntent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val idioma = getSharedPreferences("com.example.miauvet_preferences", Context.MODE_PRIVATE)
        val lang = idioma.getString("language_key", "es")!!
        setNewLocale(this, lang)
        configureNavigationDrawer(R.id.fragment_container_main)

        networkData = MiauNetworkData.getInstance(this)!!
        repositorio = MiauRepository.getInstance(this, networkData)!!

        mainVM = ViewModelProviders.of(this, MainViewModelFactory(this)).get(MainViewModel::class.java)

        val inicio = getSharedPreferences("carga_inicio", Context.MODE_PRIVATE)
        inicio.getBoolean("key", false)
        if(!inicio.getBoolean("key", false)){
            mainVM.cargaInicial()
            val editor: SharedPreferences.Editor = inicio.edit()
            editor.putBoolean("key", true)
            editor.apply()
        }

        alarmMgr = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmIntent = Intent(this, AlarmReceiver::class.java)
        pendingIntent = PendingIntent.getBroadcast(this, 100, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmMgr?.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pendingIntent)

        mainVM.cargaRazas()

        // Conexion con fragment MisMascotas, Recordatorios u Opiniones
        tipo = intent.getStringExtra("Tipo")
        misMascotasFrag = MisMascotasFragment.newInstance()
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        if(tipo == null || tipo == "0") {
            mainVM.leerMascotas()
            fragmentTransaction.replace(R.id.content_main, misMascotasFrag)
        }else if (tipo == "1"){
            mainVM.leerRecordatorios()
            fragmentTransaction.replace(R.id.content_main, recordatoriosFrag)
        }else if (tipo == "2"){
            fragmentTransaction.replace(R.id.content_main, opinionesFrag)
        }
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction.commit()
    }


    fun construirLiveDataMascotas(mascotas: ArrayList<Mascota>){
        mascotitas.postValue(mascotas)
    }

    fun construirLiveDataRecordatorios(recordatorios: ArrayList<Recordatorio>){
        recordatorcitos.postValue(recordatorios)
    }

    override fun onResume() {
        super.onResume()
        val idioma = getSharedPreferences("com.example.miauvet_preferences", Context.MODE_PRIVATE)
        val lang = idioma.getString("language_key", "es")!!
        setNewLocale(this, lang)
    }

    private fun setNewLocale(mContext: AppCompatActivity, @LocaleManager.LocaleDef language: String) {
        LocaleManager.setNewLocale(mContext, language)
    }

    fun getFAB(): FloatingActionButton{
        return fab
    }

    override fun onListFragmentInteraction(tipo: Int?) {
        val aniadirMascotaFrag = AniadirMascotasFragment.newInstance(null, null)
        val aniadirRecordatorioFrag = AniadirRecordatorioFragment.newInstance(null, null)

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        if(tipo == 1) { //Esta en Lista Mascotas
            fragmentTransaction.replace(R.id.content_main, aniadirMascotaFrag)
        } else if(tipo == 2){ //Esta en Lista Recordatorios
            fragmentTransaction.replace(R.id.content_main, aniadirRecordatorioFrag)
        }

        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction.commit()
        fragmentManager.executePendingTransactions()
    }

}
