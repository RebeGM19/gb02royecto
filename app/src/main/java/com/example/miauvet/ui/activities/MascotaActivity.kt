package com.example.miauvet.ui.activities

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import com.example.miauvet.R
import com.example.miauvet.database.MiauRepository
import com.example.miauvet.ui.fragments.*
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.database.network.MiauNetworkData
import com.example.miauvet.ui.viewmodel.MascotaViewModel
import com.example.miauvet.ui.viewmodel.MascotaViewModelFactory

class MascotaActivity : BaseActivity(), MascotaFragment.OnFragmentInteractionListener {

    lateinit var mascotaRecibida: String
    lateinit var mascotaFrag: MascotaFragment
    lateinit var editarMascotaFrag: EditarMascotaFragment
    lateinit var mascotaVM: MascotaViewModel
    var mascotita: MutableLiveData<Mascota> = MutableLiveData<Mascota>()

    /*
    *metodo que crea la activity con todos los elementos correspondientes
    * @params: savedInstanceState de tipo Bundle
    * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mascota)

        networkData = MiauNetworkData.getInstance(this)!!
        repositorio = MiauRepository.getInstance(this, networkData)!!

        mascotaVM = ViewModelProviders.of(this, MascotaViewModelFactory(this)).get(MascotaViewModel::class.java)
        mascotaVM.cargarRazas()

        //id de la mascota que ha sido seleccionada en el recyclerview
        mascotaRecibida = intent.getStringExtra("Mascota")!!
        mascotaVM.cargarMascota(mascotaRecibida.toLong())

        configureNavigationDrawer(R.id.fragment_container_mascota)

        //transicion del fragment de misMascotas al fragment de la mascota seleccionada
        mascotaFrag = MascotaFragment.newInstance(null, null)
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.content_main, mascotaFrag)
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction.commit()
    }

    fun construirLiveDataMascota(mascota: Mascota){
        mascotita.postValue(mascota)
    }

    /*metodo que contiene las distintas acciones que se podran realizar en el fragment
    * @params: posicion de tipo Int*/
    override fun onFragmentInteraction(posicion: Int) {
        editarMascotaFrag = EditarMascotaFragment.newInstance(null,null)
        val pedirConsultaFrag = PedirConsultaFragment.newInstance(null,null)
        val pedirServicioFrag = PedirServicioFragment.newInstance(null,null)
        val pedirTratamientoFrag = PedirTratamientoFragment.newInstance(null,null)

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        when(posicion) {
            2 -> {
                //editar mascota
                mascotaVM.cargarDatosMascota(mascotaRecibida.toLong())
                fragmentTransaction.replace(R.id.content_main, editarMascotaFrag)
            }
            //pedir cita
            3 -> fragmentTransaction.replace(R.id.content_main, pedirConsultaFrag)
            //pedir servicio
            4 -> fragmentTransaction.replace(R.id.content_main, pedirServicioFrag)
            //pedir tratamiento
            5 -> fragmentTransaction.replace(R.id.content_main, pedirTratamientoFrag)
            6 -> {
                //borrar mascota
                mascotaVM.borrarMascotaPorId(mascotaRecibida.toLong())
                fragmentManager.popBackStack()
                Toast.makeText(this, "Mascota eliminada", Toast.LENGTH_SHORT).show()
            }
        }
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction.commit()
        fragmentManager.executePendingTransactions()
    }

}
