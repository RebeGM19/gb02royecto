package com.example.miauvet.ui.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.miauvet.database.MiauRepository
import com.example.miauvet.database.network.MiauNetworkData

class CitasViewModel(context: Context) : ViewModel() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var repository: MiauRepository
        private var instancia: CitasViewModel? = null
        fun getInstance(context: Context): CitasViewModel? {
            if (instancia == null) {
                instancia = CitasViewModel(context)
                repository = MiauRepository.getInstance(context, MiauNetworkData.getInstance(context)!!)!!
            }
            return instancia
        }
    }

    fun getConsultas(){
        repository.listarPeticiones()
    }

    fun getServicios(){
        repository.listarServicios()
    }


}