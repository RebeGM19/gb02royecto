package com.example.miauvet.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.miauvet.R
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.ui.activities.MascotaActivity

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class EditarMascotaFragment : Fragment() {

    private var param1: String? = null
    private var param2: String? = null
    lateinit var mascota1: Mascota

    // Atributos para obtener los parámetros de la Mascota actual
    // Rellenan los campos de manera automática para que lo vea el usuario

    private  var mId: Long = 0
    private  var mNombre: String?=""
    private  var mDuenio: String? = ""
    private  var mPeso: Float=0.0F
    private  var mEspecie: String?=""
    private  var mEdad: Int=0

    private lateinit var eNombre: EditText
    private lateinit var ePeso: EditText
    private lateinit var eEspecie: EditText
    private lateinit var eEdad: EditText

    lateinit var mascotaActivity: MascotaActivity
    lateinit var spinner: Spinner

    companion object {
        @JvmStatic
        fun newInstance(param1: String?, param2: String?) =
            EditarMascotaFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_editar_mascota, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mascotaActivity = activity as MascotaActivity

        var raza = "Error" // Se cambia a través del spinner

        /* Spinner */
        spinner = view?.findViewById(R.id.DesplegableRazasEditar) as Spinner
        val adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, mascotaActivity.razas) // Meter la lista de razas en el adaptador
        spinner.adapter = adapter

        /* Listener del spinner */
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                if (view != null) { // Porque si no la API peta
                    if (pos in 0..9) { // Comprobamos el rango
                        onButtonPressed(pos) // Array empieza en 0
                    }
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            private fun onButtonPressed(i: Int) {
                raza = mascotaActivity.razas.get(i) // Coger la raza de la pos i de la lista
            }
        }

        /* ---------------------- */

        /* Boton para guardar los datos editados */

        val submitButton = view?.findViewById(R.id.botonGuardar) as Button
        submitButton.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {

                eNombre = view?.findViewById(R.id.edit_nombre)!!
                ePeso = view?.findViewById(R.id.edit_peso)!!
                eEdad = view?.findViewById(R.id.edit_edad)!!
                eEspecie = view?.findViewById(R.id.edit_especie)!!

                // Crear objeto de tipo Mascota para pasar por parametro
                mascota1 =
                    Mascota(
                        mId,
                        eNombre.text.toString(),
                        mDuenio,
                        ePeso.text.toString().toFloat(),
                        eEspecie.text.toString(),
                        raza,
                        eEdad.text.toString().toInt()
                    )

                mascotaActivity.mascotaVM.editarMascota(mascota1) // Llamamos al metodo de la mascotaActivity que se encarga de Async para introducir los datos en la BD
                mascotaActivity.supportFragmentManager.popBackStack() // Volvemos a la pantalla anterior
            }
        })

        /* ------------------------------------ */
    }

    /*
	 * Recibe los datos de la Mascota actual y los coloca en los campos de texto de manera predeterminada
	 * para facilitar al usuario la edición de los mismos
	 */
    fun camposEditarMascota(mascota: Mascota){
        var mtextview: EditText?
        var posSpinner = 0

        mId = mascota.id
        mDuenio = mascota.duenio

        mtextview = view?.findViewById(R.id.edit_nombre)
        mtextview?.setText(mascota.nombre_mascota)
        mNombre=mascota.nombre_mascota

        mtextview = view?.findViewById(R.id.edit_peso)
        mtextview?.setText(mascota.peso.toString())
        mPeso=mascota.peso

        mtextview = view?.findViewById(R.id.edit_especie)
        mtextview?.setText(mascota.especie)
        mEspecie=mascota.especie

        /* Buscar en la lista la posicion de la raza que entra por parametros para colocar el spinner bien */
        for (i in 0 until mascotaActivity.razas.size) {
            if (mascota.raza == mascotaActivity.razas[i]) // Si la raza que llega coincide con una de la lista
                posSpinner = i
        }
        spinner.setSelection(posSpinner)
        /* --------------------- */

        mtextview = view?.findViewById(R.id.edit_edad)
        mtextview?.setText(mascota.edad.toString())
        mEdad=mascota.edad
    }
}



