package com.example.miauvet.ui.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.miauvet.database.MiauRepository
import com.example.miauvet.database.network.MiauNetworkData
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.database.objects.Peticion

class MascotaViewModel(context: Context) : ViewModel() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var repository: MiauRepository
        private var instancia: MascotaViewModel? = null
        fun getInstance(context: Context): MascotaViewModel? {
            if (instancia == null) {
                instancia = MascotaViewModel(context)
                repository = MiauRepository.getInstance(context, MiauNetworkData.getInstance(context)!!)!!
            }
            return instancia
        }
    }

    fun cargarRazas(){
        //Si hay conexion a internet, coger datos de la api y guardar en cache. Sino leer de cache
        repository.cargarRazasDeApi()
    }

    /*
    * metodo que muestra una mascota determinada
    * */
    fun cargarMascota(id: Long){
        repository.seleccionarMascota(id)
    }

    /*
    *metodo que edita la mascota segun los datos que se le pasan por parametro
    * @params: mascota de tipo Mascota*/
    fun editarMascota(mascota: Mascota){
        repository.modificarMascota(mascota)
    }

    fun cargarDatosMascota(id: Long){
        repository.datosMascota(id)
    }

    fun borrarMascotaPorId(id: Long){
        repository.borrarMascota(id)
    }

    /*
    * metodo que inserta una peticion en la base de datos
    * @params: peticion de tipo Peticion
    * */
    fun guardarPeticion(peticion: Peticion){
        repository.insertarPeticion(peticion)
    }



}