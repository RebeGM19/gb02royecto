package com.example.miauvet.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.miauvet.R
import com.example.miauvet.database.objects.Recordatorio
import com.example.miauvet.ui.activities.MainActivity

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

@Suppress("NAME_SHADOWING")
class AniadirRecordatorioFragment: Fragment() {

    lateinit var recordatorio: Recordatorio
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var titulo: EditText
    private lateinit var contenido: EditText

    companion object {
        @JvmStatic
        fun newInstance(param1: String?, param2: String?) =
            AniadirRecordatorioFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_aniadir_recordatorio, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mainActivity = activity as MainActivity // Hacer invisible boton flotante
        mainActivity.getFAB().visibility = View.INVISIBLE

        // Obtener campos de texto
        titulo = view?.findViewById(R.id.titulo_recordatorio_enter) as EditText
        contenido = view?.findViewById(R.id.contenido_recordatorio_enter) as EditText

        /* Boton para guardar el Recordatorio */

        val submitButton = view?.findViewById(R.id.botonGuardar) as Button
        submitButton.setOnClickListener(object: View.OnClickListener {
            override fun onClick (v: View?) {

                // Crear objeto de tipo Recordatorio para pasar por parametro
                recordatorio =
                    Recordatorio(
                        0,
                        titulo.text.toString(),
                        contenido.text.toString()
                    )

                val mainActivity = activity as MainActivity
                mainActivity.mainVM.guardarRecordatorio(recordatorio) // Llamamos al metodo de la mainActivity que se encarga de Async para introducir los datos en la BD
                mainActivity.supportFragmentManager.popBackStack() // Volvemos a la pantalla anterior
                Toast.makeText(activity, "Recordatorio añadido", Toast.LENGTH_SHORT).show()
            }
        })

        /* ---------------------------------- */
    }
}