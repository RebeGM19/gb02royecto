package com.example.miauvet.ui.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.helpers.MisMascotasAdapter
import com.example.miauvet.R
import com.example.miauvet.ui.activities.MainActivity

import kotlinx.android.synthetic.main.fragment_lista_mascotas.*

class MisMascotasFragment : Fragment() {

    private var listener: OnListFragmentInteractionListener? = null

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(tipo: Int?)
    }

    /*
    * Metodo que crea la activity con todos los elementos correspondientes
    * @params: savedInstanceState de tipo Bundle
    */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    /*
    * Metodo llamado para que el fragmento instancie su vista de interfaz de usuario
    * @params: inflater de tipo LayoutInflater, container de tipo ViewGroup y savedInstanceState de tipo Bundle
    * @return: View
    */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        return inflater.inflate(R.layout.fragment_lista_mascotas, container, false)
    }

    /*
    * Metodo que muestra la lista de los distintos elementos del recyclerview
    * @params: mascotas que es una lista de mascotas
    */
    fun load(mascotas: List<Mascota>) {
        list.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = MisMascotasAdapter(activity, mascotas)
        }
    }

    fun cargarLista(mascotas: List<Mascota>){
        val mainActivity = activity as MainActivity
        mainActivity.construirLiveDataMascotas(mascotas as ArrayList<Mascota>)
    }

    /*
    * Metodo que se llama cuando se ha creado la actividad del fragmento
    * @params: savedInstanceState de tipo Bundle
    */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mainActivity = activity as MainActivity
        //se hace visible el boton de añadir
        mainActivity.getFAB().visibility = View.VISIBLE

        //se muestran las mascotas en el recyclerview
        mainActivity.mascotitas.observe(this, object: Observer<ArrayList<Mascota>> {
            override fun onChanged(mascotas: ArrayList<Mascota>) {
                load(mascotas)
            }
        })

        //se añade una nueva mascota pulsando el boton de añadir
        val aniadirMascota = mainActivity.getFAB()
        aniadirMascota.setOnClickListener(object: View.OnClickListener {
            override fun onClick (v: View?) {
                onButtonPressed(1) //Tipo 1: Lista Mascotas
            }
        })
    }


    fun onButtonPressed(tipo: Int?) {
        if (listener != null){
            listener?.onListFragmentInteraction(tipo)
        }
    }

    /*
    * Se llama cuando un fragmento se adjunta por primera vez a su contexto
    * @params: context de tipo Context
    */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    /*
    *Se llama cuando el fragmento ya no está unido a su actividad
    */
    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        fun newInstance(): MisMascotasFragment =
            MisMascotasFragment()
    }
}
