package com.example.miauvet.ui.fragments

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.miauvet.R
import com.example.miauvet.database.objects.Peticion
import com.example.miauvet.helpers.DatePickerFragment
import com.example.miauvet.helpers.TimePickerFragment
import com.example.miauvet.ui.activities.MascotaActivity

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class PedirServicioFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    var selectedDate: String = "00/00/0000"
    var selectedHour: String = "00:00"
    var selectedDateFin: String = "00/00/0000"
    var selectedHourFin: String = "00:00"

    companion object {
        @JvmStatic
        fun newInstance(param1: String?, param2: String?) =
            PedirServicioFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    /*
   *metodo que crea la activity con todos los elementos correspondientes
   * @params: savedInstanceState de tipo Bundle
   * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    /*metodo llamado para que el fragmento instancie su vista de interfaz de usuario
  * @params: inflater de tipo LayoutInflater, container de tipo ViewGroup y savedInstanceState de tipo Bundle
  * @return: View */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pedir_servicio, container, false)
    }

    /*metodo que se llama cuando se ha creado la actividad del fragmento
   * @params: savedInstanceState de tipo Bundle*/
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //boton para seleccionar una fecha de inicio
        val datePickerButton = view?.findViewById(R.id.elegirDiaServicio) as EditText
        datePickerButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                showDatePickerDialog(datePickerButton, 1)
            }
        })
        //boton para seleccionar una hora de inicio
        val timePickerButton = view?.findViewById(R.id.elegirHoraServicio) as EditText
        timePickerButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                showTimePickerDialog(timePickerButton, 1)
            }
        })
        //boton para seleccionar una fecha de fin
        val dateFinPickerButton = view?.findViewById(R.id.elegirDiaFinServicio) as EditText
        dateFinPickerButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                showDatePickerDialog(dateFinPickerButton, 2)
            }
        })
        //boton para seleccionar una hora de fin
        val timeFinPickerButton = view?.findViewById(R.id.elegirHoraFinServicio) as EditText
        timeFinPickerButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                showTimePickerDialog(timeFinPickerButton, 2)
            }
        })

        val mascotaActivity = activity as MascotaActivity
        val aniadirServicioButton = view?.findViewById(R.id.BotonInsertarServicio) as Button

        //boton que guarda el servicio
        aniadirServicioButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val timestamp = "$selectedDate $selectedHour"
                val timestampFin = "$selectedDateFin $selectedHourFin"
                val peticion = Peticion(
                    0,
                    3,
                    mascotaActivity.mascotaRecibida.toLong(),
                    timestamp,
                    timestampFin,
                    null)
                mascotaActivity.mascotaVM.guardarPeticion(peticion)
                Toast.makeText(activity, "Servicio guardado $timestamp $timestampFin", Toast.LENGTH_SHORT).show()
                mascotaActivity.supportFragmentManager.popBackStack()
            }
        })
    }

    /*metodo que permite cambiar el formato de la fecha
    * @params: anio de tipo Int, mes de tipo Int y dia de tipo Int
    * @return String*/
    fun formatearFecha(anio: Int, mes: Int, dia: Int): String{
        var mesAux: String = mes.toString()
        var diaAux: String = dia.toString()
        if(mes < 10){
            mesAux = "0$mes"
        }
        if(dia < 10){
            diaAux = "0$dia"
        }
        return "$anio-$mesAux-$diaAux"
    }

    /*metodo que permite cambiar el formato de la hora
    * @params: hora de tipo Int y minuto de tipo Int
    * @return String*/
    fun formatearHora(hora: Int, minuto: Int): String{
        var minutoAux: String = minuto.toString()
        var horaAux: String = hora.toString()
        if(minuto < 10){
            minutoAux = "0$minuto"
        }
        if(hora < 10){
            horaAux = "0$hora"
        }
        return "$horaAux:$minutoAux:00"
    }

    /*
    * metodo que muestra el calendario para elegir una fecha
    */
    fun showDatePickerDialog(datePickerButton: EditText, tipo:Int) {
        when(tipo) {
            1 -> {
                val newFragment =
                    DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener { _, year, month, day ->
                        // +1 porque Enero es 0
                        selectedDate = formatearFecha(year, month + 1, day)
                        datePickerButton.setText(selectedDate)
                    })
                fragmentManager?.let { newFragment.show(it, "datePicker") }
            }
            2 -> {
                val newFragment =
                    DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener { _, year, month, day ->
                        // +1 porque Enero es 0
                        selectedDateFin = formatearFecha(year, month + 1, day)
                        datePickerButton.setText(selectedDateFin)
                    })
                fragmentManager?.let { newFragment.show(it, "datePicker") }
            }
        }
    }

    /*
   * metodo que muestra el reloj para elegir una hora
   */
    fun showTimePickerDialog(timePickerButton: EditText, tipo:Int) {
        when(tipo) {
            1 -> {
                val newFragment =
                    TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                        selectedHour = formatearHora(hour, minute)
                        timePickerButton.setText(selectedHour)
                    })
                fragmentManager?.let { newFragment.show(it, "datePicker") }
            }
            2 -> {
                val newFragment =
                    TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                        selectedHourFin = formatearHora(hour, minute)
                        timePickerButton.setText(selectedHourFin)
                    })
                fragmentManager?.let { newFragment.show(it, "datePicker") }
            }
        }
    }
}
