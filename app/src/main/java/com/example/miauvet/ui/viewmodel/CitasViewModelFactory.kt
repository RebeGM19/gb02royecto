package com.example.miauvet.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CitasViewModelFactory: ViewModelProvider.Factory {

    private var context: Context

    constructor (context: Context) {
        this.context = context
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CitasViewModel.getInstance(this.context) as T
    }
}