package com.example.miauvet.ui.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.miauvet.R
import com.example.miauvet.database.objects.Recordatorio
import com.example.miauvet.helpers.RecordatoriosAdapter
import kotlinx.android.synthetic.main.fragment_recordatorio.*
import com.example.miauvet.ui.activities.MainActivity

class RecordatorioFragment : Fragment() {

    private var listener: OnListFragmentInteractionListener? = null

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(tipo: Int?)
    }

    /*
    * metodo que crea la activity con todos los elementos correspondientes
    * @params: savedInstanceState de tipo Bundle
    */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    /*
    * metodo llamado para que el fragmento instancie su vista de interfaz de usuario
    * @params: inflater de tipo LayoutInflater, container de tipo ViewGroup y savedInstanceState de tipo Bundle
    * @return: View
    */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        return inflater.inflate(R.layout.fragment_recordatorio, container, false)
    }

    /*
    * metodo para mostra la lista de recordatorios del recyclerview
    * @params: recordatorios lista de recordatorios
    */
    fun load(recordatorios: List<Recordatorio>) {
        list_recordatorio.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = RecordatoriosAdapter(activity, recordatorios)
        }
    }

    fun cargarListaR(recordatorios: List<Recordatorio>){
        val mainActivity = activity as MainActivity
        mainActivity.construirLiveDataRecordatorios(recordatorios as ArrayList<Recordatorio>)
    }

    /*
    * metodo que se llama cuando se ha creado la actividad del fragmento
    * @params: savedInstanceState de tipo Bundle
    */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mainActivity = activity as MainActivity
        mainActivity.getFAB().visibility = View.VISIBLE
        //muestra los recordatorios
        mainActivity.recordatorcitos.observe(this, object: Observer<ArrayList<Recordatorio>> {
            override fun onChanged(recordatorios: ArrayList<Recordatorio>) {
                load(recordatorios)
            }
        })

        val aniadirRecordatorio = mainActivity.getFAB()
        aniadirRecordatorio.setOnClickListener(object: View.OnClickListener {
            override fun onClick (v: View?) {
                onButtonPressed(2) //Tipo 2: Lista Recordatorios
            }
        })
    }

    fun onButtonPressed(tipo: Int?) {
        if (listener != null){
            listener?.onListFragmentInteraction(tipo)
        }
    }

    /*
    * Se llama cuando un fragmento se adjunta por primera vez a su contexto
    * @params: context de tipo Context
    */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    /*
    * Se llama cuando el fragmento ya no está unido a su actividad
    *
    */
    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        fun newInstance(): RecordatorioFragment =
            RecordatorioFragment()
    }
}
