package com.example.miauvet.ui.fragments


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.miauvet.R
import com.example.miauvet.database.objects.Peticion
import com.example.miauvet.helpers.CitasAdapter
import com.example.miauvet.ui.activities.CitasActivity
import kotlinx.android.synthetic.main.fragment_mostrar_consultas.*

class MostrarTratamientosFragment : Fragment() {
    private var listener: OnListFragmentInteractionListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mostrar_tratamientos, container, false)
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(pos: Int?)
    }

    fun loadTratamientos(peticiones: List<Peticion>) {
        val citas: MutableList<Peticion> = ArrayList<Peticion>()
        for(i in 0..peticiones.size-1){
            if(peticiones.get(i).tipo == 2){
                citas.add(peticiones.get(i))
            }
        }
        lista_citas.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = CitasAdapter(activity, citas)
        }
    }

    fun cargarTratamientos(consultas: List<Peticion>){
        val citas: MutableList<Peticion> = ArrayList<Peticion>()
        for(i in 0..consultas.size-1){
            if(consultas.get(i).tipo == 2){
                citas.add(consultas.get(i))
            }
        }
        val citasActivity = activity as CitasActivity
        citasActivity.construirLiveDataPeticiones(citas as ArrayList<Peticion>)
    }

    /*
   * Se llama cuando un fragmento se adjunta por primera vez a su contexto
   * @params: context de tipo Context
   */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    /*
    * Se llama cuando el fragmento ya no está unido a su actividad
    *
    */
    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    companion object {
        fun newInstance(): MostrarTratamientosFragment =
            MostrarTratamientosFragment()
    }
}
