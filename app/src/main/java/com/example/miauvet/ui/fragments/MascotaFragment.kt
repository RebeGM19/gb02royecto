package com.example.miauvet.ui.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import com.example.miauvet.R
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.ui.activities.MascotaActivity

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class MascotaFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    companion object {
        @JvmStatic
        fun newInstance(param1: String?, param2: String?) =
            MascotaFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mascota, container, false)
    }

    fun cargarMascotita(mascota: Mascota){
        val mascotaActivity = activity as MascotaActivity
        mascotaActivity.construirLiveDataMascota(mascota)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val mascotaActivity = activity as MascotaActivity

        // Buscar los botones y asignarles listeners para desplazarse entre fragmentos
        val editarMascota2 = view?.findViewById(R.id.Modificar) as Button
        val borrarMascota4 = view?.findViewById(R.id.Borrar) as Button

        val spinner = view?.findViewById(R.id.DesplegablePedir) as Spinner
        ArrayAdapter.createFromResource(context!!,
            R.array.spinner_mascota, android.R.layout.simple_spinner_item).also { adapter ->
            // Estilo de lista del spinner
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Aplicar el adaptador que permita la comunicacion con el spinner
            spinner.adapter = adapter
        }

        /* Listener del spinner */
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                if (view != null) { // Porque si no la API peta
                    if (pos in 1..3){ // Comprobamos el rango
                        spinner.setSelection(0) // Devolver el spinner al item original
                        onButtonPressed(pos + 2) // Array empieza en 0 y nuestros frag son 3-4-5
                    }
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
        /* -------------------- */

        /* Listener de botones */
        editarMascota2.setOnClickListener(object: View.OnClickListener {
            override fun onClick (v: View?) {
                onButtonPressed(2)
            }
        })
        borrarMascota4.setOnClickListener(object: View.OnClickListener {
            override fun onClick (v: View?) {
                onButtonPressed(6)
                mascotaActivity.finish()
            }
        })
        /* ------------------ */

        mascotaActivity.mascotita.observe(this, object: Observer<Mascota> {
            override fun onChanged(mascota: Mascota) {
                mostrarMascota(mascota)
            }
        })
    }

    fun onButtonPressed(posicion: Int) {
        if (listener != null) {
            listener?.onFragmentInteraction(posicion)
        }
    }

    /*
     * Recibe los datos de la Mascota actual y los coloca en los campos de texto
     */
    fun mostrarMascota(mascota: Mascota){
        var mtextview: TextView?

        mtextview = view?.findViewById(R.id.mNombre)
        mtextview?.text = mascota.nombre_mascota

        mtextview = view?.findViewById(R.id.mPeso)
        mtextview?.text = mascota.peso.toString()

        mtextview = view?.findViewById(R.id.mEspecie)
        mtextview?.text = mascota.especie

        mtextview = view?.findViewById(R.id.mRaza)
        mtextview?.text = mascota.raza

        mtextview = view?.findViewById(R.id.mEdad)
        mtextview?.text = mascota.edad.toString()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(posicion: Int)
    }
}
