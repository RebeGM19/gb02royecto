package com.example.miauvet.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import android.content.pm.PackageManager
import android.content.pm.PackageManager.GET_META_DATA
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.FloatingActionButton

// Para la BD
import com.example.miauvet.helpers.LocaleManager
import com.example.miauvet.R
import com.example.miauvet.database.MiauRepository
import com.example.miauvet.database.objects.Opinion
import com.example.miauvet.helpers.SectionsPagerAdapter
import com.example.miauvet.database.network.MiauNetworkData
import com.example.miauvet.ui.fragments.*
import com.google.android.material.tabs.TabLayout

// Para el JSON
import java.util.ArrayList


abstract class BaseActivity: AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    lateinit var fab : FloatingActionButton

    lateinit var networkData: MiauNetworkData
    lateinit var repositorio: MiauRepository

    // Fragments
    lateinit var misMascotasFrag: MisMascotasFragment

    lateinit var consultasFrag: MostrarConsultasFragment
    lateinit var tratamientosFrag: MostrarTratamientosFragment
    lateinit var serviciosFrag: MostrarServiciosFragment

    var recordatoriosFrag: RecordatorioFragment = RecordatorioFragment.newInstance()
    var opinionesFrag: OpinionFragment = OpinionFragment.newInstance(null, null)

    // Listas
    var razas: MutableList<String> = ArrayList<String>() // Utilizada para dar datos a los spinners

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        resetTitles()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleManager.setLocale(base))
    }

    /*
     * Para las preferencias de idioma, reconstruir etiquetas
     */
    fun resetTitles() {
        try {
            val info = packageManager.getActivityInfo(componentName, GET_META_DATA)
            if (info.labelRes != 0) {
                setTitle(info.labelRes)
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    fun configureNavigationDrawer(id: Int){
        // Activamos la toolbar
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        fab = findViewById(R.id.floatingActionButton)

        //Navigation Drawer
        drawerLayout = findViewById(id)
        navView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        val tabs = findViewById(R.id.tabs) as TabLayout
        if(this.toString().contains("CitasActivity")){
            val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
            val viewPager: ViewPager = findViewById(R.id.view_pager)
            viewPager.adapter = sectionsPagerAdapter
            tabs.setupWithViewPager(viewPager)
            consultasFrag = sectionsPagerAdapter.mostrarConsultasFragment
            tratamientosFrag = sectionsPagerAdapter.mostrarTratamientosFragment
            serviciosFrag = sectionsPagerAdapter.mostrarServiciosFragment
        }else{
            tabs.visibility = View.INVISIBLE
        }
    }

    /*
     * Metodo que gestiona el comportamiento del navigation drawer
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if(item.itemId != R.id.nav_ajustes) {
            val fragmentManager = supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            when (item.itemId) {
                R.id.nav_mascotas -> {
                    fab.visibility = View.VISIBLE
                    if(this.toString().contains("MainActivity")) {
                        val mainActivity: MainActivity = this as MainActivity
                        mainActivity.mainVM.leerMascotas()
                        fragmentTransaction.replace(R.id.content_main, misMascotasFrag)
                    }else{
                        val intent = Intent(this, MainActivity::class.java)
                        intent.putExtra("Tipo", "0")
                        this.startActivity(intent)
                    }
                }
                R.id.nav_Opiniones -> {
                    fab.visibility = View.INVISIBLE
                    if(this.toString().contains("MainActivity")) {
                        fragmentTransaction.replace(R.id.content_main, opinionesFrag)
                    }else{
                        val intent = Intent(this, MainActivity::class.java)
                        intent.putExtra("Tipo", "2")
                        this.startActivity(intent)
                    }
                }
                R.id.nav_recordatorios -> {
                    fab.visibility = View.INVISIBLE
                    if(this.toString().contains("MainActivity")) {
                        val mainActivity: MainActivity = this as MainActivity
                        mainActivity.mainVM.leerRecordatorios()
                        fragmentTransaction.replace(R.id.content_main, recordatoriosFrag)
                    }else{
                        val intent = Intent(this, MainActivity::class.java)
                        intent.putExtra("Tipo", "1")
                        this.startActivity(intent)
                    }
                }
                R.id.nav_citas -> {
                    fab.visibility = View.INVISIBLE
                        val intent = Intent(this, CitasActivity::class.java)
                        this.startActivity(intent)
                }
            }
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }else{
            val ajustes = Intent(this, SettingsActivity::class.java)
            startActivity(ajustes)
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    fun guardarOpinion(opinion: Opinion){
        repositorio.insertarOpinion(opinion)
    }
}