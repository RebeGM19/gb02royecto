package com.example.miauvet.database.objects

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "Opinion")
class Opinion {

    @PrimaryKey(autoGenerate = true)
    var id: Long
    var titulo: String? = null
    var puntuacion: Float? = null

    @Ignore
    constructor() {
        this.id = 0
        this.titulo = ""
        this.puntuacion = -1.0f
    }

    constructor(id: Long, titulo: String?, puntuacion: Float?) {
        this.id = id
        this.titulo = titulo
        this.puntuacion = puntuacion
    }

    fun setId(ID: Long?) {
        this.id = ID!!
    }

}