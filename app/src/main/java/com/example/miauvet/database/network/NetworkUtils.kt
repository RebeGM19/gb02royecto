package com.example.miauvet.database.network

import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "JAVA_CLASS_ON_COMPANION")
class NetworkUtils {

    companion object{

        private fun getResponse(url: URL): String{
            var urlConnection: HttpURLConnection? = null
            val bufferedReader: BufferedReader
            var line: String?
            val stringBuilder: StringBuilder = StringBuilder()

            try{
                urlConnection = url.openConnection() as HttpURLConnection?
                bufferedReader = BufferedReader(InputStreamReader(urlConnection?.inputStream))


                line = bufferedReader.readLine()
                while (line != null){
                    stringBuilder.append(line)
                    line = bufferedReader.readLine()
                }

                if(urlConnection?.responseCode != 200){
                    Log.e(NetworkUtils.javaClass.name, "Error retrieving connection message")
                }
            } catch (e: Exception){
                Log.e(NetworkUtils.javaClass.name, "Error de conexion con la API")
            } finally {
                if(urlConnection != null){
                    urlConnection.disconnect()
                }
            }
            return stringBuilder.toString()
        }

        fun getJSONResponse(url: URL): JSONObject?{
            val result: String = getResponse(url)
            if(!result.equals("null")){
                try {
                    return JSONObject(result)
                }catch (e: JSONException){
                    Log.e(NetworkUtils.javaClass.name, "Error de creacion de JSON")
                }
            }
            return null
        }

    }

}