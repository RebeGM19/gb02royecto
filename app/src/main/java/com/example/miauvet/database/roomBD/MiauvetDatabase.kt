package com.example.miauvet.database.roomBD

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.database.objects.Opinion
import com.example.miauvet.database.objects.Peticion
import com.example.miauvet.database.objects.Recordatorio


@Database(entities = [Mascota::class, Peticion::class, Opinion::class, Recordatorio::class], version = 1, exportSchema = false)
abstract class MiauvetDatabase : RoomDatabase() {

    companion object {
        private var instance: MiauvetDatabase? = null
        fun getDataBase(context: Context): MiauvetDatabase? {
            if (instance == null) {
                instance = Room.databaseBuilder(context.applicationContext, MiauvetDatabase::class.java, "mascotas.db").build()
            }
            return instance
        }
    }

    abstract fun mascotaDAO(): MascotaDAO
    abstract fun peticionDAO(): PeticionDAO
    abstract fun recordatorioDAO(): RecordatorioDAO
    abstract fun opinionDAO(): OpinionDAO

}