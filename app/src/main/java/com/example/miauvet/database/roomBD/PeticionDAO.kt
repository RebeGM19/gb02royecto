package com.example.miauvet.database.roomBD

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.miauvet.database.objects.Peticion

//interfaz mediante la cual se podran realizar las acciones deseadas en la base de datos
@Dao
interface PeticionDAO {

    /*
   * metodo que inserta una peticion en la base de datos
   * @params: item de tipo peticion
   * @return devuelve un tipo Long
   * */
    @Insert
    fun insert(item: Peticion): Long

    /*
   * metodo que devuelve todas las peticiones en una lista
   * */
    @get:Query("SELECT * FROM Peticion")
    val selectAll: List<Peticion>


}