package com.example.miauvet.database.objects

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import com.example.miauvet.database.roomBD.DateConverter
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

//Definición de la tabla Peticion mediante la clase del mismo nombre, utiliza sus atributos para referenciar las columnas de la tabla
@Entity(tableName = "Peticion")
class Peticion {

       //variable que permite el formateo de fecha y hora
        @Ignore
        val FORMAT = SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss", Locale.US
        )

        //Se establece el id como clave primaria de la tabla
        //variable que guarda el identificador de la peticion
        @PrimaryKey(autoGenerate = true)
        var id: Long

        //variable que almacena el tipo de la peticion
        var tipo: Int? = null

        //se establece la mascota como clave foranea para relacionar la peticion con la mascota correspondiente
        @ForeignKey(entity = Mascota::class, parentColumns = arrayOf("tipo"), childColumns = arrayOf("id"), onDelete = CASCADE)
        var mascota: Long = -1

        //variable dia que almacena la fecha de la petición, se le aplicará un converter para cambiar el formato
        @TypeConverters(DateConverter::class)
        var dia: Date? = null

        //variable que se utilizará si es una petición de tipo guardería, se le aplicará un converter para cambiar el formato
        @TypeConverters(DateConverter::class)
        var diafin: Date? = null

        //variable que almacena el tipo de petición que se realizará
        var tipotratamiento: Int? = null

    /*
    * constructor por defecto de la clase Peticion
    * */
        @Ignore
        constructor() {
            this.id = 0
            this.tipo = null
            this.mascota = -1
            this.dia = null
            this.diafin = null
            this.tipotratamiento = null
        }

    /*
    * constructor parametrizado de la clase Peticion
    * @params: id de tipo Long, tipo que sera Int, mascota de tipo Long, dia de tipo String, diafin de tipo String y tipotratamiento de tipo Int
    * */
        @Ignore
        constructor(id: Long, tipo: Int?, mascota: Long, dia: String?, diafin: String?, tipotratamiento: Int?) {
            this.id = id
            this.tipo = tipo
            this.mascota = mascota
            if (dia != null){
                try {
                    this.dia = this.FORMAT.parse(dia)
                } catch (e: ParseException) {
                    this.dia = Date()
                }
            }
            if(diafin != null) {
                try {
                    this.diafin = this.FORMAT.parse(diafin)
                } catch (e: ParseException) {
                    this.diafin = Date()
                }
            }
            this.tipotratamiento = tipotratamiento
        }

    /*
  * constructor parametrizado de la clase Peticion
  * @params: id de tipo Long, tipo que sera Int, mascota de tipo Long, dia de tipo Date, diafin de tipo Date y tipotratamiento de tipo Int
  * */
        constructor(id: Long, tipo: Int?, mascota: Long, dia: Date?, diafin: Date?, tipotratamiento: Int?) {
            this.id = id
            this.tipo= tipo
            this.mascota = mascota
            this.dia = dia
            this.diafin = diafin
            this.tipotratamiento = tipotratamiento
        }

    /*
    * metodo que modifica el atributo id de la clase por el pasado por parametro
    * @params: id de tipo Long
    * */
        fun setId(ID: Long?) {
            this.id = ID!!
        }

}