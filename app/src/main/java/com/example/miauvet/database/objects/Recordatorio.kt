package com.example.miauvet.database.objects

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

//Definición de la tabla Recordatorio mediante la clase del mismo nombre, utiliza sus atributos para referenciar las columnas de la tabla
@Entity(tableName = "Recordatorio")
class Recordatorio {

    //Se establece el id como clave primaria de la tabla
    //variable que guarda el identificador del recordatorio
    @PrimaryKey(autoGenerate = true)
    var id: Long

    //variable que guarda el titulo del recordatorio
    var titulo: String? = null

    //variable que guarda el texto del recordatorio
    var contenido: String? = null

    /*
   * constructor por defecto de la clase Recordatorio
   * */
    @Ignore
    constructor() {
        this.id = 0
        this.titulo = ""
        this.contenido = ""
    }

    /*
   * constructor parametrizado de la clase Recordatorio
   * @params: id de tipo Long, titulo de tipo String y contenido de tipo String
   * */
    constructor(id: Long, titulo: String?, contenido: String?) {
        this.id = id
        this.titulo = titulo
        this.contenido = contenido
    }

    /*
   * metodo que modifica el atributo id de la clase por el pasado por parametro
   * @params: id de tipo Long
   * */
    fun setId(ID: Long?) {
        this.id = ID!!
    }

}