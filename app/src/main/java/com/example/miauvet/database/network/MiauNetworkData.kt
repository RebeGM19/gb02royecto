package com.example.miauvet.database.network

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.miauvet.database.objects.Mascota
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList
import java.net.URL

class MiauNetworkData {

    private lateinit var mContext: Context
    private lateinit var mMascotasLiveData: MutableLiveData<Array<Mascota>>

    companion object {
        private var LOCK: Any = Object()
        @SuppressLint("StaticFieldLeak")
        private var instancia: MiauNetworkData? = null
        fun getInstance(context: Context): MiauNetworkData? {
            if (instancia == null) {
                synchronized(LOCK) {
                    instancia = MiauNetworkData()
                    instancia!!.mContext = context
                    instancia!!.mMascotasLiveData = MutableLiveData<Array<Mascota>>()
                }
            } else {
                instancia!!.mContext = context
            }
            return instancia
        }
    }

    fun jsonToRazas(responseObject: JSONObject): List<String>{
        val result: MutableList<String> = ArrayList<String>()
        try {
            val contenido: JSONArray = responseObject.getJSONArray("razas")
            for(i in 0 until contenido.length()){
                val raza: JSONObject = contenido.get(i) as JSONObject
                result.add(raza.get("name").toString())
            }
        }catch (e: JSONException){
            e.printStackTrace()
        }
        return result
    }

    fun jsonToMascotas(responseObject: JSONObject): MutableLiveData<ArrayList<Mascota>> {
        val result: MutableLiveData<ArrayList<Mascota>> = MutableLiveData<ArrayList<Mascota>>()
        val aux: ArrayList<Mascota> = ArrayList<Mascota>()
        try {
            val contenido: JSONArray = responseObject.getJSONArray("mascotas")
            for(i in 0 until contenido.length()){
                val mascota: JSONObject = contenido.get(i) as JSONObject
                val id = mascota.getLong("id")
                val nombre_mascota = mascota.getString("nombre_mascota")
                val duenio = mascota.getString("duenio")
                val peso = mascota.getDouble("peso").toFloat()
                val especie = mascota.getString("especie")
                val raza = mascota.getString("raza")
                val edad = mascota.getInt("edad")
                val mascotaResul = Mascota(id, nombre_mascota, duenio, peso, especie, raza, edad)
                aux.add(mascotaResul)
            }
            result.postValue(aux)
        }catch (e: JSONException){
            e.printStackTrace()
        }
        return result
    }

    fun conseguirMascotas(): MutableLiveData<ArrayList<Mascota>>{
        var listaMascotas: MutableLiveData<ArrayList<Mascota>> = MutableLiveData<ArrayList<Mascota>>()
        try {
            val queryUrl = URL("https://api.myjson.com/bins/bkzlw")
            val result: JSONObject? = NetworkUtils.getJSONResponse(queryUrl)
            if(result != null){
                listaMascotas = jsonToMascotas(result)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return listaMascotas
    }

    fun conseguirRazas(): List<String>{
        var listaRazas: MutableList<String> = ArrayList<String>()
        try {
            val queryUrl = URL("https://api.myjson.com/bins/15l8py")
            val result: JSONObject? = NetworkUtils.getJSONResponse(queryUrl)
            if(result != null){
                listaRazas = jsonToRazas(result).toMutableList()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return listaRazas
    }

}