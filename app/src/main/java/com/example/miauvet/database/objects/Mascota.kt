package com.example.miauvet.database.objects

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "Mascota")
class Mascota {

    @PrimaryKey(autoGenerate = true)
    var id: Long
    var nombre_mascota: String? = null
    var duenio: String? = null
    var peso: Float = 0.0f
    var especie: String? = null
    var raza: String? = null
    var edad: Int = 0

    @Ignore
    constructor() {
        this.id = 0
        this.nombre_mascota = ""
        this.duenio = ""
        this.peso = 0.0f
        this.especie = ""
        this.raza = ""
        this.edad = 0
    }

    constructor(id: Long, nombre_mascota: String?, duenio: String?, peso: Float, especie: String?, raza: String?, edad: Int) {
        this.id = id
        this.nombre_mascota = nombre_mascota
        this.duenio = duenio
        this.peso = peso
        this.especie = especie
        this.raza = raza
        this.edad = edad
    }

    fun setId(ID: Long?) {
        this.id = ID!!
    }

}