package com.example.miauvet.database

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.miauvet.database.network.MiauNetworkData
import com.example.miauvet.database.objects.Mascota
import com.example.miauvet.database.objects.Opinion
import com.example.miauvet.database.objects.Peticion
import com.example.miauvet.database.objects.Recordatorio
import com.example.miauvet.database.roomBD.MiauvetDatabase
import com.example.miauvet.ui.activities.CitasActivity
import com.example.miauvet.ui.activities.MainActivity
import com.example.miauvet.ui.activities.MascotaActivity

class MiauRepository {

    private lateinit var miauDatabase: MiauvetDatabase
    private lateinit var miauNetworkData: MiauNetworkData
    private lateinit var miauContext: Context

    private lateinit var mainActivity: MainActivity
    private lateinit var mascotaActivity: MascotaActivity
    private lateinit var citasActivity: CitasActivity
    var listaMascotas: MutableLiveData<ArrayList<Mascota>> = MutableLiveData<ArrayList<Mascota>>()
    var listaRecordatorios: MutableLiveData<ArrayList<Recordatorio>> = MutableLiveData<ArrayList<Recordatorio>>()
    private lateinit var mascotaAux: Mascota

    companion object {
        private var LOCK: Any = Object()
        @SuppressLint("StaticFieldLeak")
        private var instancia: MiauRepository? = null
        fun getInstance(context: Context, networkData: MiauNetworkData): MiauRepository? {
            if (instancia == null) {
                synchronized(LOCK) {
                    instancia = MiauRepository()
                    instancia!!.miauContext = context
                    instancia!!.miauDatabase = MiauvetDatabase.getDataBase(context)!!
                    instancia!!.miauNetworkData = MiauNetworkData.getInstance(context)!!
                    if (context.toString().contains("MainActivity")) {
                        instancia!!.mainActivity = context as MainActivity
                    }
                    if (context.toString().contains("MascotaActivity")) {
                        instancia!!.mascotaActivity = context as MascotaActivity
                    }
                    if (context.toString().contains("CitasActivity")) {
                        instancia!!.citasActivity = context as CitasActivity
                    }
                }
            } else {
                instancia!!.miauContext = context
                instancia!!.miauNetworkData = MiauNetworkData.getInstance(context)!!
                if (context.toString().contains("MainActivity")) {
                    instancia?.mainActivity = context as MainActivity
                }
                if (context.toString().contains("MascotaActivity")) {
                    instancia?.mascotaActivity = context as MascotaActivity
                }
                if (context.toString().contains("CitasActivity")) {
                    instancia?.citasActivity = context as CitasActivity
                }
            }
            return instancia
        }
    }

    @SuppressLint("StaticFieldLeak")
    private inner class AsyncCargarMascotasDeApi : AsyncTask<Void, Void,  MutableLiveData<ArrayList<Mascota>>>() {
        override fun doInBackground(vararg voids: Void): MutableLiveData<ArrayList<Mascota>> {
            return miauNetworkData.conseguirMascotas()
        }
        override fun onPostExecute(result: MutableLiveData<ArrayList<Mascota>>) {
            super.onPostExecute(result)
            listaMascotas = result
        }
    }
    @SuppressLint("StaticFieldLeak")
    private inner class AsyncBuscarMascota : AsyncTask<MutableLiveData<ArrayList<Mascota>>, Void, Boolean>() {
        override fun doInBackground(vararg id: MutableLiveData<ArrayList<Mascota>>?): Boolean {
            var mascota: Mascota?
            for(i in 0..listaMascotas.value!!.size -1) {
                mascota = miauDatabase.mascotaDAO().selectMascota(listaMascotas.value!!.get(i).id)
                if (mascota == null) {
                    miauDatabase.mascotaDAO().insert(listaMascotas.value!!.get(i))
                }
            }
            return true
        }
    }
    fun selectMascotasDeApi(){
        AsyncCargarMascotasDeApi().execute()
        AsyncBuscarMascota().execute(listaMascotas)
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncCargarRazasDeApi : AsyncTask<Void, Void, List<String>>() {
        public override fun doInBackground(vararg voids: Void): List<String> {
            return miauNetworkData.conseguirRazas()
        }
        override fun onPostExecute(result: List<String>) {
            if(miauContext.toString().contains("MainActivity")) {
                mainActivity.razas = result.toMutableList()
            }
            if(miauContext.toString().contains("MascotaActivity")) {
                mascotaActivity.razas = result.toMutableList()
            }
        }
    }
    fun cargarRazasDeApi(){
        AsyncCargarRazasDeApi().execute()
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncLeerMascotas : AsyncTask<Void, Void, List<Mascota>>() {
        override fun doInBackground(vararg voids: Void): List<Mascota> {
            return miauDatabase.mascotaDAO().selectAll
        }
        override fun onPostExecute(items: List<Mascota>) {
            super.onPostExecute(items)
            if (listaMascotas.value != null) {
                listaMascotas.value!!.clear()
                for (i in 0..items.size - 1) {
                    listaMascotas.value!!.add(items.get(i))
                }
            } else {
                listaMascotas.value = items as ArrayList<Mascota>
            }
            mainActivity.misMascotasFrag.cargarLista(items)
            mainActivity.misMascotasFrag.load(items)

        }
    }
    fun leerMascotas(){
        AsyncLeerMascotas().execute()
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncInsertarMascota : AsyncTask<Mascota, Void, Mascota>() {
        override fun doInBackground(vararg mascota: Mascota): Mascota {
            val id = miauDatabase.mascotaDAO().insert(mascota[0])
            mascota[0].setId(id)
            return mascota[0]
        }
        override fun onPostExecute(mascota: Mascota) {
            super.onPostExecute(mascota)
            listaMascotas.value!!.add(mascota)
            mainActivity.misMascotasFrag.cargarLista(listaMascotas.value!!)
        }
    }
    fun insertarMascota(mascota: Mascota){
        AsyncInsertarMascota().execute(mascota)
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncBorrarMascotaPorID : AsyncTask<Long, Void, Long>() {
        override fun doInBackground(vararg id: Long?): Long? {
            miauDatabase.mascotaDAO().borrarMascotaPorId(id[0]!!)
            return id[0]!!
        }
        override fun onPostExecute(id: Long) {
            super.onPostExecute(id)
            var enc = false
            var i = 0
            while (i < listaMascotas.value!!.size && !enc){
                if(listaMascotas.value!!.get(i).id == id){
                    enc = true
                    listaMascotas.value!!.removeAt(i)
                }
                i++
            }
            mainActivity.misMascotasFrag.cargarLista(listaMascotas.value!!)
        }
    }
    fun borrarMascota(id: Long){
        AsyncBorrarMascotaPorID().execute(id)
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncSelectMascota : AsyncTask<Long, Void, Mascota>() {
        override fun doInBackground(vararg id: Long?): Mascota? {
            return miauDatabase.mascotaDAO().selectMascota(id[0]!!)
        }
        override fun onPostExecute(mascota: Mascota) {
            super.onPostExecute(mascota)
            mascotaAux = mascota
            mascotaActivity.mascotaFrag.cargarMascotita(mascota)
            mascotaActivity.mascotaFrag.mostrarMascota(mascota)
        }
    }
    fun seleccionarMascota(id: Long){
        AsyncSelectMascota().execute(id)
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncSelectEditar : AsyncTask<Long, Void, Mascota>() {
        override fun doInBackground(vararg id: Long?): Mascota? {
            return miauDatabase.mascotaDAO().selectMascota(id[0]!!)
        }
        override fun onPostExecute(mascota: Mascota) {
            super.onPostExecute(mascota)
            mascotaAux = mascota
            mascotaActivity.editarMascotaFrag.camposEditarMascota(mascotaAux) // Llamar al metodo que coloca los parametros de la mascota en el fragment EditarMascota
        }
    }
    fun datosMascota(id: Long){
        AsyncSelectEditar().execute(id)
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncEditMascota : AsyncTask<Mascota, Void, Mascota>() {
        override fun doInBackground(vararg mascota: Mascota): Mascota {
            miauDatabase.mascotaDAO().updateStatus(mascota[0])
            return mascota[0]
        }
        override fun onPostExecute(mascota: Mascota) {
            super.onPostExecute(mascota)
            for (i in 0 until  listaMascotas.value!!.size) {
                if (mascota.id == listaMascotas.value!!.get(i).id) {
                    listaMascotas.value!!.get(i).nombre_mascota = mascota.nombre_mascota
                    listaMascotas.value!!.get(i).peso = mascota.peso
                    listaMascotas.value!!.get(i).especie = mascota.especie
                    listaMascotas.value!!.get(i).raza = mascota.raza
                    listaMascotas.value!!.get(i).edad = mascota.edad
                }
            }
            mainActivity.misMascotasFrag.cargarLista(listaMascotas.value!!)
            mascotaActivity.mascotaFrag.cargarMascotita(mascota)
            mascotaActivity.editarMascotaFrag.camposEditarMascota(mascota)

        }
    }
    fun modificarMascota(mascota: Mascota){
        AsyncEditMascota().execute(mascota)
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncInsertarOpinion : AsyncTask<Opinion, Void, Opinion>() {
        private lateinit var opiniones: List<Opinion>
        override fun doInBackground(vararg opinion: Opinion): Opinion {
            val id = miauDatabase.opinionDAO().insert(opinion[0])
            opinion[0].setId(id)
            opiniones = miauDatabase.opinionDAO().selectAll
            return opinion[0]
        }
        override fun onPostExecute(result: Opinion?) {
            super.onPostExecute(result)
            for(i in 0..opiniones.size-1){
                Log.i("Opiniones", opiniones.get(i).id.toString() + " " + opiniones.get(i).titulo.toString() + " " + opiniones.get(i).puntuacion.toString())
            }
        }
    }
    fun insertarOpinion(opinion: Opinion){
        AsyncInsertarOpinion().execute(opinion)
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncLeerRecordatorios : AsyncTask<Void, Void, List<Recordatorio>>() {
        override fun doInBackground(vararg voids: Void): List<Recordatorio> {
            val lista = miauDatabase.recordatorioDAO().selectAll
            return lista
        }
        override fun onPostExecute(items: List<Recordatorio>) {
            super.onPostExecute(items)
            listaRecordatorios.value = items as ArrayList<Recordatorio>
            mainActivity.recordatoriosFrag.cargarListaR(items)
            mainActivity.recordatoriosFrag.load(items)
        }
    }
    fun listarRecordatorios(){
        AsyncLeerRecordatorios().execute()
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncInsertarRecordatorio : AsyncTask<Recordatorio, Void, Recordatorio>() {
        override fun doInBackground(vararg recordatorio: Recordatorio): Recordatorio {
            val id = miauDatabase.recordatorioDAO().insert(recordatorio[0])
            recordatorio[0].setId(id)
            return recordatorio[0]
        }
        override fun onPostExecute(result: Recordatorio?) {
            super.onPostExecute(result)
            listaRecordatorios.value!!.add(result!!)
            mainActivity.recordatoriosFrag.cargarListaR(listaRecordatorios.value!!)
        }
    }
    fun insertarRecordatorio(recordatorio: Recordatorio){
        AsyncInsertarRecordatorio().execute(recordatorio)
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncInsertarPeticion : AsyncTask<Peticion, Void, Peticion>() {
        override fun doInBackground(vararg peticion: Peticion): Peticion {
            val id = miauDatabase.peticionDAO().insert(peticion[0])
            peticion[0].setId(id)
            return peticion[0]
        }
    }
    fun insertarPeticion(peticion: Peticion){
        AsyncInsertarPeticion().execute(peticion)
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncLeerPeticiones : AsyncTask<Void, Void, List<Peticion>>() {
        override fun doInBackground(vararg voids: Void): List<Peticion> {
            return miauDatabase.peticionDAO().selectAll
        }
        override fun onPostExecute(items: List<Peticion>) {
            super.onPostExecute(items)
            citasActivity.consultasFrag.cargarConsultas(items)
            citasActivity.tratamientosFrag.cargarTratamientos(items)
            citasActivity.consultasFrag.loadCitas(items)
            citasActivity.tratamientosFrag.loadTratamientos(items)
        }
    }
    fun listarPeticiones(){
        AsyncLeerPeticiones().execute()
    }


    @SuppressLint("StaticFieldLeak")
    private inner class AsyncLeerServicios : AsyncTask<Void, Void, List<Peticion>>() {
        override fun doInBackground(vararg voids: Void): List<Peticion> {
            return miauDatabase.peticionDAO().selectAll
        }
        override fun onPostExecute(items: List<Peticion>) {
            super.onPostExecute(items)
            citasActivity.serviciosFrag.cargarServicios(items)
            citasActivity.serviciosFrag.loadServicios(items)
        }
    }
    fun listarServicios(){
        AsyncLeerServicios().execute()
    }



}