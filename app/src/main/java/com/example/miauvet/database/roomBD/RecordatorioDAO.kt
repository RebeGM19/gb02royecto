package com.example.miauvet.database.roomBD

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.miauvet.database.objects.Recordatorio

//interfaz mediante la cual se podran realizar las acciones deseadas en la base de datos
@Dao
interface RecordatorioDAO {

    /*
  * metodo que inserta un recordatorio en la base de datos
  * @params: item de tipo recordatorio
  * @return devuelve un tipo Long
  * */
    @Insert
    fun insert(item: Recordatorio): Long

    /*
  * metodo que devuelve todas los recordatorios en una lista
  * */
    @get:Query("SELECT * FROM Recordatorio")
    val selectAll: List<Recordatorio>

}