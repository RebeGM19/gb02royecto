package com.example.miauvet.database.roomBD

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.miauvet.database.objects.Opinion

//interfaz mediante la cual se podran realizar las acciones deseadas en la base de datos
@Dao
interface OpinionDAO {

    /*
    * metodo que inserta una opinion en la base de datos
    * @params: item de tipo opinion
    * @return devuelve un tipo Long
    * */
    @Insert
    fun insert(item: Opinion): Long

    /*
    * metodo que devuelve todas las opiniones en una lista
    * */
    @get:Query("SELECT * FROM Opinion")
    val selectAll: List<Opinion>
}