package com.example.miauvet.database.roomBD

import androidx.room.*
import com.example.miauvet.database.objects.Mascota

@Dao
interface MascotaDAO {

    @get:Query("SELECT * FROM Mascota")
    val selectAll: List<Mascota>

    @Insert
    fun insert(item: Mascota): Long

    @Update
    fun updateStatus(item: Mascota): Int

    @Query("SELECT * FROM Mascota WHERE id = :id LIMIT 1")
    fun selectMascota(id: Long): Mascota?

    @Query("DELETE FROM Mascota WHERE id = :id")
    fun borrarMascotaPorId(id: Long): Int

}